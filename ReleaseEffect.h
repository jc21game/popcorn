#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class ReleaseEffect : public GameObject
{
	int hPict_;    //画像番号 

public:
	//コンストラクタ
	ReleaseEffect(GameObject* parent);

	//デストラクタ
	~ReleaseEffect();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};