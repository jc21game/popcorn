#include "ResultScene.h"
#include "PlayScene_C.h"
#include "PlayScene_B.h"
#include "PlayScene_S.h"
#include "SelectUI.h"
#include "Score_C.h"
#include "Score_B.h"
#include "Score_S.h"
#include "ResultScore.h"
#include "LockManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"



//コンストラクタ
ResultScene::ResultScene(GameObject * parent)
  : GameObject(parent, "ResultScene"),bgpict_(-1),UnLockpict_(-1)
{
	for (int i = 0; i < 10; i++)
	{
		hPict_[i] = -1;
		tenhPict_[i] = -1;
		hundhPict_[i] = -1;
		thoushPict_[i] = -1;
		tenthoushPict_[i] = -1;
		hund_thous_hPict_[i] = -1;
		millionhPict_[i] = -1;
	}

	for (int i = 0; i < 7; i++)
	{
		aScore_[i] = 0;
	}
}

//初期化
void ResultScene::Initialize()
{
	for (int i = 0; i < 10; i++)
	{
		switch (i)
		{
		case 0:
			hPict_[i] = Image::Load("zero.png");
			tenhPict_[i] = Image::Load("zero.png");
			hundhPict_[i] = Image::Load("zero.png");
			thoushPict_[i] = Image::Load("zero.png");
			tenthoushPict_[i] = Image::Load("zero.png");
			hund_thous_hPict_[i] = Image::Load("zero.png");
			millionhPict_[i] = Image::Load("zero.png");
			break;
		case 1:
			hPict_[i] = Image::Load("one.png");
			tenhPict_[i] = Image::Load("one.png");
			hundhPict_[i] = Image::Load("one.png");
			thoushPict_[i] = Image::Load("one.png");
			tenthoushPict_[i] = Image::Load("one.png");
			hund_thous_hPict_[i] = Image::Load("one.png");
			millionhPict_[i] = Image::Load("one.png");
			break;
		case 2:
			hPict_[i] = Image::Load("two.png");
			tenhPict_[i] = Image::Load("two.png");
			hundhPict_[i] = Image::Load("two.png");
			thoushPict_[i] = Image::Load("two.png");
			tenthoushPict_[i] = Image::Load("two.png");
			hund_thous_hPict_[i] = Image::Load("two.png");
			millionhPict_[i] = Image::Load("two.png");
			break;
		case 3:
			hPict_[i] = Image::Load("three.png");
			tenhPict_[i] = Image::Load("three.png");
			hundhPict_[i] = Image::Load("three.png");
			thoushPict_[i] = Image::Load("three.png");
			tenthoushPict_[i] = Image::Load("three.png");
			hund_thous_hPict_[i] = Image::Load("three.png");
			millionhPict_[i] = Image::Load("three.png");
			break;
		case 4:
			hPict_[i] = Image::Load("four.png");
			tenhPict_[i] = Image::Load("four.png");
			hundhPict_[i] = Image::Load("four.png");
			thoushPict_[i] = Image::Load("four.png");
			tenthoushPict_[i] = Image::Load("four.png");
			hund_thous_hPict_[i] = Image::Load("four.png");
			millionhPict_[i] = Image::Load("four.png");
			break;
		case 5:
			hPict_[i] = Image::Load("five.png");
			tenhPict_[i] = Image::Load("five.png");
			hundhPict_[i] = Image::Load("five.png");
			thoushPict_[i] = Image::Load("five.png");
			tenthoushPict_[i] = Image::Load("five.png");
			hund_thous_hPict_[i] = Image::Load("five.png");
			millionhPict_[i] = Image::Load("five.png");
			break;
		case 6:
			hPict_[i] = Image::Load("six.png");
			tenhPict_[i] = Image::Load("six.png");
			hundhPict_[i] = Image::Load("six.png");
			thoushPict_[i] = Image::Load("six.png");
			tenthoushPict_[i] = Image::Load("six.png");
			hund_thous_hPict_[i] = Image::Load("six.png");
			millionhPict_[i] = Image::Load("six.png");
			break;
		case 7:
			hPict_[i] = Image::Load("seven.png");
			tenhPict_[i] = Image::Load("seven.png");
			hundhPict_[i] = Image::Load("seven.png");
			thoushPict_[i] = Image::Load("seven.png");
			tenthoushPict_[i] = Image::Load("seven.png");
			hund_thous_hPict_[i] = Image::Load("seven.png");
			millionhPict_[i] = Image::Load("seven.png");
			break;
		case 8:
			hPict_[i] = Image::Load("eight.png");
			tenhPict_[i] = Image::Load("eight.png");
			hundhPict_[i] = Image::Load("eight.png");
			thoushPict_[i] = Image::Load("eight.png");
			tenthoushPict_[i] = Image::Load("eight.png");
			hund_thous_hPict_[i] = Image::Load("eight.png");
			millionhPict_[i] = Image::Load("eight.png");
			break;
		case 9:
			hPict_[i] = Image::Load("nine.png");
			tenhPict_[i] = Image::Load("nine.png");
			hundhPict_[i] = Image::Load("nine.png");
			thoushPict_[i] = Image::Load("nine.png");
			tenthoushPict_[i] = Image::Load("nine.png");
			hund_thous_hPict_[i] = Image::Load("nine.png");
			millionhPict_[i] = Image::Load("nine.png");
		}
		assert(hPict_[i] >= 0);
		assert(tenhPict_[i] >= 0);
		assert(hundhPict_[i] >= 0);
		assert(thoushPict_[i] >= 0);
		assert(tenthoushPict_[i] >= 0);
		assert(hund_thous_hPict_[i] >= 0);
		assert(millionhPict_[i] >= 0);
	}
	//スコア(文字)を表示
	bgpict_ = Image::Load("Score.png");
	assert(bgpict_ >= 0);

	transform_.position_ = XMVectorSet(0.24f, 0, 0, 0);
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	tentrans_.position_ = XMVectorSet(0.16f, 0, 0, 0);
	tentrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	hundtrans_.position_ = XMVectorSet(0.08f, 0, 0, 0);
	hundtrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);
	thoustrans_.position_ = XMVectorSet(0, 0, 0, 0);
	thoustrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	tenthoustrans_.position_ = XMVectorSet(-0.08f, 0, 0, 0);
	tenthoustrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	hund_thous_trans_.position_ = XMVectorSet(-0.16f, 0, 0, 0);
	hund_thous_trans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	milliontrans_.position_ = XMVectorSet(-0.24f, 0, 0, 0);
	milliontrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);

	randam = rand() % 8 + 1;
	Flow = true;

}

//更新
void ResultScene::Update()
{
	SceneJudg();

	if (flag == 1)
	{
		aScore_[0] = ResultScore::resultScore_C / 1000000;
		aScore_[1] = ResultScore::resultScore_C % 1000000 / 1000000;
		aScore_[2] = ResultScore::resultScore_C % 100000 / 10000;
		aScore_[3] = ResultScore::resultScore_C % 10000 / 1000;
		aScore_[4] = ResultScore::resultScore_C % 1000 / 100;
		aScore_[5] = ResultScore::resultScore_C % 100 / 10;
		aScore_[6] = ResultScore::resultScore_C % 10;

		if (ResultScore::resultScore_C == 0)
		{
			n = 0;
			t = 0;
			h = 0;
			th = 0;
			tt = 0;
			ht = 0;
			m = 0;
		}

		switch (aScore_[6])
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9;
		}

		switch (aScore_[5])
		{
		case 0:t = 0; break;
		case 1:t = 1; break;
		case 2:t = 2; break;
		case 3:t = 3; break;
		case 4:t = 4; break;
		case 5:t = 5; break;
		case 6:t = 6; break;
		case 7:t = 7; break;
		case 8:t = 8; break;
		case 9:t = 9;
		}

		switch (aScore_[4])
		{
		case 0:h = 0; break;
		case 1:h = 1; break;
		case 2:h = 2; break;
		case 3:h = 3; break;
		case 4:h = 4; break;
		case 5:h = 5; break;
		case 6:h = 6; break;
		case 7:h = 7; break;
		case 8:h = 8; break;
		case 9:h = 9;
		}

		switch (aScore_[3])
		{
		case 0:th = 0; break;
		case 1:th = 1; break;
		case 2:th = 2; break;
		case 3:th = 3; break;
		case 4:th = 4; break;
		case 5:th = 5; break;
		case 6:th = 6; break;
		case 7:th = 7; break;
		case 8:th = 8; break;
		case 9:th = 9;
		}

		switch (aScore_[2])
		{
		case 0:tt = 0; break;
		case 1:tt = 1; break;
		case 2:tt = 2; break;
		case 3:tt = 3; break;
		case 4:tt = 4; break;
		case 5:tt = 5; break;
		case 6:tt = 6; break;
		case 7:tt = 7; break;
		case 8:tt = 8; break;
		case 9:tt = 9;
		}

		switch (aScore_[1])
		{
		case 0:ht = 0; break;
		case 1:ht = 1; break;
		case 2:ht = 2; break;
		case 3:ht = 3; break;
		case 4:ht = 4; break;
		case 5:ht = 5; break;
		case 6:ht = 6; break;
		case 7:ht = 7; break;
		case 8:ht = 8; break;
		case 9:ht = 9;
		}

		switch (aScore_[0])
		{
		case 0:m = 0; break;
		case 1:m = 1; break;
		case 2:m = 2; break;
		case 3:m = 3; break;
		case 4:m = 4; break;
		case 5:m = 5; break;
		case 6:m = 6; break;
		case 7:m = 7; break;
		case 8:m = 8; break;
		case 9:m = 9;
		}
	}
	else if (flag == 2)
	{
		aScore_[0] = ResultScore::resultScore_B / 1000000;
		aScore_[1] = ResultScore::resultScore_B % 1000000 / 1000000;
		aScore_[2] = ResultScore::resultScore_B % 100000 / 10000;
		aScore_[3] = ResultScore::resultScore_B % 10000 / 1000;
		aScore_[4] = ResultScore::resultScore_B % 1000 / 100;
		aScore_[5] = ResultScore::resultScore_B % 100 / 10;
		aScore_[6] = ResultScore::resultScore_B % 10;

		if (ResultScore::resultScore_B == 0)
		{
			n = 0;
			t = 0;
			h = 0;
			th = 0;
			tt = 0;
			ht = 0;
			m = 0;
		}
		switch (aScore_[6])
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9;
		}

		switch (aScore_[5])
		{
		case 0:t = 0; break;
		case 1:t = 1; break;
		case 2:t = 2; break;
		case 3:t = 3; break;
		case 4:t = 4; break;
		case 5:t = 5; break;
		case 6:t = 6; break;
		case 7:t = 7; break;
		case 8:t = 8; break;
		case 9:t = 9;
		}

		switch (aScore_[4])
		{
		case 0:h = 0; break;
		case 1:h = 1; break;
		case 2:h = 2; break;
		case 3:h = 3; break;
		case 4:h = 4; break;
		case 5:h = 5; break;
		case 6:h = 6; break;
		case 7:h = 7; break;
		case 8:h = 8; break;
		case 9:h = 9;
		}

		switch (aScore_[3])
		{
		case 0:th = 0; break;
		case 1:th = 1; break;
		case 2:th = 2; break;
		case 3:th = 3; break;
		case 4:th = 4; break;
		case 5:th = 5; break;
		case 6:th = 6; break;
		case 7:th = 7; break;
		case 8:th = 8; break;
		case 9:th = 9;
		}

		switch (aScore_[2])
		{
		case 0:tt = 0; break;
		case 1:tt = 1; break;
		case 2:tt = 2; break;
		case 3:tt = 3; break;
		case 4:tt = 4; break;
		case 5:tt = 5; break;
		case 6:tt = 6; break;
		case 7:tt = 7; break;
		case 8:tt = 8; break;
		case 9:tt = 9;
		}

		switch (aScore_[1])
		{
		case 0:ht = 0; break;
		case 1:ht = 1; break;
		case 2:ht = 2; break;
		case 3:ht = 3; break;
		case 4:ht = 4; break;
		case 5:ht = 5; break;
		case 6:ht = 6; break;
		case 7:ht = 7; break;
		case 8:ht = 8; break;
		case 9:ht = 9;
		}

		switch (aScore_[0])
		{
		case 0:m = 0; break;
		case 1:m = 1; break;
		case 2:m = 2; break;
		case 3:m = 3; break;
		case 4:m = 4; break;
		case 5:m = 5; break;
		case 6:m = 6; break;
		case 7:m = 7; break;
		case 8:m = 8; break;
		case 9:m = 9;
		}
	}

	else if (flag == 3)
	{
	aScore_[0] = ResultScore::resultScore_S / 1000000;
	aScore_[1] = ResultScore::resultScore_S % 1000000 / 1000000;
	aScore_[2] = ResultScore::resultScore_S % 100000 / 10000;
	aScore_[3] = ResultScore::resultScore_S % 10000 / 1000;
	aScore_[4] = ResultScore::resultScore_S % 1000 / 100;
	aScore_[5] = ResultScore::resultScore_S % 100 / 10;
	aScore_[6] = ResultScore::resultScore_S % 10;

	if (ResultScore::resultScore_S == 0)
	{
		n = 0;
		t = 0;
		h = 0;
		th = 0;
		tt = 0;
		ht = 0;
		m = 0;
	}

	switch (aScore_[6])
	{
	case 0:n = 0; break;
	case 1:n = 1; break;
	case 2:n = 2; break;
	case 3:n = 3; break;
	case 4:n = 4; break;
	case 5:n = 5; break;
	case 6:n = 6; break;
	case 7:n = 7; break;
	case 8:n = 8; break;
	case 9:n = 9;
	}

	switch (aScore_[5])
	{
	case 0:t = 0; break;
	case 1:t = 1; break;
	case 2:t = 2; break;
	case 3:t = 3; break;
	case 4:t = 4; break;
	case 5:t = 5; break;
	case 6:t = 6; break;
	case 7:t = 7; break;
	case 8:t = 8; break;
	case 9:t = 9;
	}

	switch (aScore_[4])
	{
	case 0:h = 0; break;
	case 1:h = 1; break;
	case 2:h = 2; break;
	case 3:h = 3; break;
	case 4:h = 4; break;
	case 5:h = 5; break;
	case 6:h = 6; break;
	case 7:h = 7; break;
	case 8:h = 8; break;
	case 9:h = 9;
	}

	switch (aScore_[3])
	{
	case 0:th = 0; break;
	case 1:th = 1; break;
	case 2:th = 2; break;
	case 3:th = 3; break;
	case 4:th = 4; break;
	case 5:th = 5; break;
	case 6:th = 6; break;
	case 7:th = 7; break;
	case 8:th = 8; break;
	case 9:th = 9;
	}

	switch (aScore_[2])
	{
	case 0:tt = 0; break;
	case 1:tt = 1; break;
	case 2:tt = 2; break;
	case 3:tt = 3; break;
	case 4:tt = 4; break;
	case 5:tt = 5; break;
	case 6:tt = 6; break;
	case 7:tt = 7; break;
	case 8:tt = 8; break;
	case 9:tt = 9;
	}

	switch (aScore_[1])
	{
	case 0:ht = 0; break;
	case 1:ht = 1; break;
	case 2:ht = 2; break;
	case 3:ht = 3; break;
	case 4:ht = 4; break;
	case 5:ht = 5; break;
	case 6:ht = 6; break;
	case 7:ht = 7; break;
	case 8:ht = 8; break;
	case 9:ht = 9;
	}

	switch (aScore_[0])
	{
	case 0:m = 0; break;
	case 1:m = 1; break;
	case 2:m = 2; break;
	case 3:m = 3; break;
	case 4:m = 4; break;
	case 5:m = 5; break;
	case 6:m = 6; break;
	case 7:m = 7; break;
	case 8:m = 8; break;
	case 9:m = 9;
	}
	}

	


	if (Flow)
	{
		switch (randam)
		{
		case 1:
			switch (flag)
			{
			case 1:if (ResultScore::MaxCombo_C > 20) LockManager::Lock = true; Flow = false; break;
			case 2:if (ResultScore::MaxCombo_B > 15) LockManager::Lock = true; Flow = false; break;
			case 3:if (ResultScore::MaxCombo_S > 14) LockManager::Lock = true; Flow = false;
			}break;
		case 2:
			switch (flag)
			{
			case 1:if (ResultScore::resultScore_C > 500) LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 2:if (ResultScore::resultScore_B > 50) LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 3:if (ResultScore::resultScore_S > 70) LockManager::Lock = true; LockManager::disLock = true;
			}break;
			//case 3://各核難易度のコーンを中心でとらえた回数を参照
		case 4:
			switch (flag)
			{
			case 1:if (ResultScore::MaxCombo_C > 20)  LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 2:if (ResultScore::MaxCombo_B > 40)  LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 3:if (ResultScore::MaxCombo_S > 60)  LockManager::Lock = true; LockManager::disLock = true; Flow = false;
			}break;
		case 5:
			switch (flag)
			{
			case 1:if (ResultScore::resultScore_C > 30)  LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 2:if (ResultScore::resultScore_B > 50)  LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 3:if (ResultScore::resultScore_S > 70)  LockManager::Lock = true; LockManager::disLock = true; Flow = false;
			}break;
			//case 6://各核難易度のコーンを中心でとらえた回数を参照
		case 7:
			switch (flag)
			{
			case 1:if (ResultScore::MaxCombo_C > 20) LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 2:if (ResultScore::MaxCombo_B > 40) LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 3:if (ResultScore::MaxCombo_S > 60) LockManager::Lock = true; LockManager::disLock = true; Flow = false;
			}break;
		case 8:
			switch (flag)
			{
			case 1:if (ResultScore::resultScore_C > 30)  LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 2:if (ResultScore::resultScore_B > 50)  LockManager::Lock = true; LockManager::disLock = true; Flow = false; break;
			case 3:if (ResultScore::resultScore_S > 70)  LockManager::Lock = true; LockManager::disLock = true; Flow = false;
			}break;
			//case 9://各核難易度のコーンを中心でとらえた回数を参照
		}
	}
	//マウスが押されたらシーンを切り替える
	if(Input::IsMouseButtonDown(0))
	{
			//シーンの切り替え->ホームシーンへ
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_SELECT);
	}
}

//描画
void ResultScene::Draw()
{
	//スコア(枠)の表示
	Image::SetTransform(bgpict_, bgtrans_);
	Image::Draw(bgpict_);
	//スコア(数字)の表示
	Image::SetTransform(hPict_[n], transform_);
	Image::Draw(hPict_[n]);
	Image::SetTransform(tenhPict_[t], tentrans_);
	Image::Draw(tenhPict_[t]);
	Image::SetTransform(hundhPict_[h], hundtrans_);
	Image::Draw(hundhPict_[h]);
	Image::SetTransform(thoushPict_[th], thoustrans_);
	Image::Draw(thoushPict_[th]);
	Image::SetTransform(tenthoushPict_[tt], tenthoustrans_);
	Image::Draw(tenthoushPict_[tt]);
	Image::SetTransform(hund_thous_hPict_[ht], hund_thous_trans_);
	Image::Draw(hund_thous_hPict_[ht]);
	Image::SetTransform(millionhPict_[m], milliontrans_);
	Image::Draw(millionhPict_[m]);
}

//開放
void ResultScene::Release()
{
}

//どのシーンのリザルトか
void ResultScene::SceneJudg()
{
	if (ResultScore::resultScore_C != 0)
	{
		flag = 1;
	}
	else if (ResultScore::resultScore_B != 0)
	{
		flag = 2;
	}
	else if (ResultScore::resultScore_S != 0)
	{
		flag = 3;
	}
}