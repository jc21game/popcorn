#include "Butter.h"
#include "PlayScene_B.h"
#include "Score_B.h"
#include "ResultScore.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"
#include "Engine/Direct3D.h"
#include "Engine/Transform.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Butter::Butter(GameObject * parent)
	:GameObject(parent, "Butter"), hSound_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Butter::~Butter()
{
}

//初期化
void Butter::Initialize()
{
	PlayScene_B* pPlayB = (PlayScene_B*)FindObject("PlayScene_B");
	hPict_[0] = Image::Load("Corn.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("PopCorn.png");
	assert(hPict_[1] >= 0);
	//サウンドデータのロード
	hSound_ = Audio::Load("sound.wav");
	assert(hSound_ >= 0);
	transform_.position_.vecY = 1.0f;
	//コーンが降ってくる位置をランダムにする
	switch (pPlayB->line_)
	{
	case 1:rNumber_ = rand() % 2 - 9; break; //-9 〜 -8
	case 2:rNumber_ = rand() % 4 - 5; break; //-5 〜 -2
	case 3:rNumber_ = rand() % 3 + 2; break; //2 〜 4
	case 4:rNumber_ = rand() % 3 + 7;//7 〜 9
	}
	
	transform_.position_.vecX = rNumber_ * 0.1f;
	

	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);


}

//更新
void Butter::Update()
{
	if (flag)
	{
		if (++Count_ > 5)
		{
			KillMe();
		}
	}
	transform_.position_.vecY -= CornSpeed_;  //コーンを落とす
	PlayScene_B* pPlayB = (PlayScene_B*)FindObject("PlayScene_B");
	Score_B* pScoreB = (Score_B*)FindObject("Score_B");

	//マウス(左)が押されたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの座標を取得
		MousePos_ = Input::GetMousePosition();
		//カップよりも上の場所で押されているか(Judge関数はReleaseの下に記載しています)
		if (MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 245 && Judge(MousePos_, transform_.position_) && pPlayB->clickflag_)
		{
			Audio::Play(hSound_);
			pPlayB->CountUp();
			flag_ = 1;
			ResultScore::Popcorn -= 1;

			if (pPlayB->GetComboCount() < 20)
			{
				pScoreB->ScoreUp(1);
			}
			else if (pPlayB->GetComboCount() >= 20 && pPlayB->GetComboCount() < 50)
			{
				pScoreB->ScoreUp(2);
			}
			else if (pPlayB->GetComboCount() >= 50 && pPlayB->GetComboCount() <= 100)
			{
				pScoreB->ScoreUp(3);
			}

			flag = true;

		}

	}

	//弾がある程度、下に行ったら消える
	if (transform_.position_.vecY < -1.0f)
	{
		pPlayB->ZeroCount();
		KillMe();
	}


}

//描画
void Butter::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Butter::Release()
{
}

//マウスとコーンの当たり判定
bool Butter::Judge(XMVECTOR mouse, XMVECTOR Butter)
{
	if ((mouse.vecX - ConversionX(Butter.vecX)) *(mouse.vecX - ConversionX(Butter.vecX)) + (mouse.vecY - ConversionY(Butter.vecY)) * (mouse.vecY - ConversionY(Butter.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float Butter::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xButterCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xButterCenter_;
	}
	else if (xPos > 0)
	{
		xButterCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xButterCenter_;
	}
	else if (xPos == 0)
	{
		xButterCenter_ = DispCenter_.vecX;
		return xButterCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float Butter::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		yButterCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return yButterCenter_;
	}
	else if (yPos < 0)
	{
		yButterCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return yButterCenter_;
	}
	else if (yPos == 0)
	{
		yButterCenter_ = DispCenter_.vecY;
		return yButterCenter_;
	}
}
