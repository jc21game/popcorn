#pragma once
#include "Engine/GameObject.h"

//Sceneryを管理するクラス
class Scenery : public GameObject
{
	int hPict_;    //画像番号

public:
	//コンストラクタ
	Scenery(GameObject* parent);

	//デストラクタ
	~Scenery();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};