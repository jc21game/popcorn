#include "EndlessScene.h"
#include "DifficultyLevel.h"
#include "BackGroundimage.h"
#include "FryingPan.h"
#include "Cup_End.h"
#include "Endless.h"
#include "Combo_E.h"
#include "Score_E.h"
#include "EndlessSpeed.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
EndlessScene::EndlessScene(GameObject * parent)
	:GameObject(parent, "EndlessScene")
{

}

//デストラクタ
EndlessScene::~EndlessScene()
{
}

//初期化
void EndlessScene::Initialize()
{


	//背景を表示させる
	Instantiate<BackGroundimage>(this);

	//フライパンを表示させる
	Instantiate<FryingPan>(this);

	//カップを表示させる
	Instantiate<Cup_End>(this);

	//コンボを表示させる
	Instantiate<Combo_E>(this);

	//スコアを表示させる
	Instantiate<Score_E>(this);


	//コーンを表示させる
	for (int i = 0; i < 3; i++)
	{
		Instantiate<Endless>(this);
		line_ += 1;
	}

}

//更新
void EndlessScene::Update()
{
	if (FindChildObject("Endless") == nullptr)
	{
		if (DeadFlag_ < 3)
		{
			pRandom = rand() % 9 + 1;
			switch (pRandom)
			{
			case 1:
				line_ = 1;
				for (int i = 0; i < 3; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 2:
				line_ = 1;
				EndlessSpeed::CornSpeed = 0.004f;
				for (int i = 0; i < 3; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 3:
				line_ = 1;
				EndlessSpeed::CornSpeed = 0.005f;
				for (int i = 0; i < 3; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 4:
				line_ = 1;
				PopCount_ = 4;
				EndlessSpeed::CornSpeed = 0.004f;
				for (int i = 0; i < 4; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 5:
				line_ = 1;
				PopCount_ = 4;
				EndlessSpeed::CornSpeed = 0.005f;
				for (int i = 0; i < 4; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 6:
				line_ = 1;
				PopCount_ = 4;
				EndlessSpeed::CornSpeed = 0.006f;
				for (int i = 0; i < 4; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 7:
				line_ = 1;
				PopCount_ = 5;
				EndlessSpeed::CornSpeed = 0.005f;
				for (int i = 0; i < 5; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 8:
				line_ = 1;
				PopCount_ = 5;
				EndlessSpeed::CornSpeed = 0.006f;
				for (int i = 0; i < 5; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
				break;
			case 9:
				line_ = 1;
				PopCount_ = 5;
				EndlessSpeed::CornSpeed = 0.007f;
				for (int i = 0; i < 5; i++)
				{
					Instantiate<Endless>(this);
					line_ += 1;
				}
			}
			
		}
		else
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_HOME);
		}
	}

}

//描画
void EndlessScene::Draw()
{

}

//解放
void EndlessScene::Release()
{
}

void EndlessScene::CountUp()
{
	CupCounter_ = CupCounter_ + 1;
	ComboCounter_ = ComboCounter_ + 1;
}

int EndlessScene::GetCount()
{
	return CupCounter_;
}

int EndlessScene::GetComboCount()
{
	return ComboCounter_;
}

void EndlessScene::ZeroCount()
{
	ComboCounter_ = 0;
}

void EndlessScene::DeadCountUp()
{
	DeadFlag_ += 1;
}



