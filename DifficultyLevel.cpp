#include "DifficultyLevel.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//コンストラクタ
DifficultyLevel::DifficultyLevel(GameObject * parent)
	:GameObject(parent, "DifficultyLevel"), ShPict_(-1), BhPict_(-1), ChPict_(-1)
{

}

//デストラクタ
DifficultyLevel::~DifficultyLevel()
{
}

//初期化
void DifficultyLevel::Initialize()
{
	//しお画像のロード
			ShPict_ = Image::Load("salt.png");
			assert(ShPict_ >= 0);
			transform_.position_.vecX = -0.45f;
			transform_.position_.vecY = -0.6f;
	//バター画像のロード
			BhPict_ = Image::Load("butter.png");
			assert(BhPict_ >= 0);
			Btransform_.position_.vecX = -0.45f;
	//キャラメル画像のロード
			ChPict_ = Image::Load("caramel.png");
			assert(ChPict_ >= 0);
			Ctransform_.position_.vecX = -0.45f;
			Ctransform_.position_.vecY =  0.6f;
			
}

//更新
void DifficultyLevel::Update()
{
	//クリックしたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの情報取得
		MousePos_ = Input::GetMousePosition();
		//画像とマウスの座標を比べる
		//キャラメル
		if (MousePos_.vecX > (Direct3D::screenWidth_ / 2 * 0.55) - 257 && MousePos_.vecX < (Direct3D::screenWidth_/2 *0.55) + 257 && MousePos_.vecY > (Direct3D::screenHeight_/2 * 0.4) - 129 && MousePos_.vecY < (Direct3D::screenHeight_/2* 0.4) + 129)
		{
			//シーンの切り替え(プレイシーンへ)
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_PLAY_C);
		}
		//バター
		if (MousePos_.vecX > (Direct3D::screenWidth_ / 2 * 0.55) - 257 && MousePos_.vecX < (Direct3D::screenWidth_ / 2 * 0.55) + 257 && MousePos_.vecY >(Direct3D::screenHeight_ / 2) - 129 && MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 129)
		{
			//シーンの切り替え(プレイシーンへ)
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_PLAY_B);
		}
		//しお
		if (MousePos_.vecX > (Direct3D::screenWidth_ / 2 * 0.55) - 257 && MousePos_.vecX < (Direct3D::screenWidth_ / 2 * 0.55) + 257 && MousePos_.vecY >(Direct3D::screenHeight_ / 2 + (Direct3D::screenHeight_/2 -Direct3D::screenHeight_/2* 0.4)) - 129 && MousePos_.vecY < (Direct3D::screenHeight_ / 2 + (Direct3D::screenHeight_ / 2 - Direct3D::screenHeight_ / 2 * 0.4) + 129))
		{
			//シーンの切り替え(プレイシーンへ)
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_PLAY_S);
		}
	}

}

//描画
void DifficultyLevel::Draw()
{
	//しおの描画
	Image::SetTransform(ShPict_, transform_);
	Image::Draw(ShPict_);
	//バターの描画
	Image::SetTransform(BhPict_, Btransform_);
	Image::Draw(BhPict_);
	//キャラメルの描画
	Image::SetTransform(ChPict_, Ctransform_);
	Image::Draw(ChPict_);
}

//解放
void DifficultyLevel::Release()
{
}
