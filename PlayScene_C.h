#pragma once
#include "Engine/GameObject.h"

class PlayScene_C : public GameObject
{
private:
	int CupCounter_ = 0;
	int ComboCounter_ = 0;
	int LoopCounter_ = 0;
	int Count_ = 0;
	
	bool flag_ = false;

	
public:
	char line_ = 1;
	bool clickflag_ = true;
	
	//コンストラクタ
	PlayScene_C(GameObject* parent);

	//デストラクタ
	~PlayScene_C();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	void CountUp();

	int GetCount();

	int GetComboCount();

	void ZeroCount();


};
