#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class Cup_C : public GameObject
{
	int hPict_[2];    //画像番号 
	char flag_ = 0;

public:
	//コンストラクタ
	Cup_C(GameObject* parent);

	//デストラクタ
	~Cup_C();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};