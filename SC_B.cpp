#include "SC_B.h"
#include "PlayScene_B.h"
#include "Score_B.h"
#include "ResultScore.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"
#include "Engine/Direct3D.h"
#include "Engine/Transform.h"
#include "Engine/SceneManager.h"

//コンストラクタ
SC_B::SC_B(GameObject * parent)
	:GameObject(parent, "SC_B"), hSound_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
SC_B::~SC_B()
{
}

//初期化
void SC_B::Initialize()
{
	PlayScene_B* pPlayB = (PlayScene_B*)FindObject("PlayScene_B");
	hPict_[0] = Image::Load("SpecialCorn.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("PopCorn.png");
	assert(hPict_[1] >= 0);
	//サウンドデータのロード
	hSound_ = Audio::Load("sound.wav");
	assert(hSound_ >= 0);

	transform_.position_.vecY = 1.0f;

	rPos_ = rand() % 2 + 1;
	//コーンが降ってくる位置をランダムにする
	switch (rPos_)
	{
	case 1:rNumber_ = rand() % 2 - 7; break;
	case 2:rNumber_ = rand() % 3 - 1; break;
	case 3:rNumber_ = rand() % 2 + 5;
	}

	transform_.position_.vecX = rNumber_ * 0.1f;

	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);
}

//更新
void SC_B::Update()
{
	if (flag)
	{
		if (++Count_ > 10)
		{
			KillMe();
		}
	}
	transform_.position_.vecY -= CornSpeed_;  //コーンを落とす
	PlayScene_B* pPlayB = (PlayScene_B*)FindObject("PlayScene_B");
	Score_B* pScoreB = (Score_B*)FindObject("Score_B");

	//マウス(左)が押されたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの座標を取得
		MousePos_ = Input::GetMousePosition();
		//カップよりも上の場所で押されているか(Judge関数はReleaseの下に記載しています)
		if (MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 245 && Judge(MousePos_, transform_.position_))
		{
			Audio::Play(hSound_);
			flag_ = 1;
			pScoreB->ScoreUp(500);
			pPlayB->clickflag_ = false;
			flag = true;

		}
	}

	//弾がある程度、下に行ったら消える
	if (transform_.position_.vecY < -1.0f)
	{
		KillMe();
	}

	if (ResultScore::Popcorn < 0)
	{
		KillMe();
	}
}

//描画
void SC_B::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void SC_B::Release()
{
}

//マウスとコーンの当たり判定
bool SC_B::Judge(XMVECTOR mouse, XMVECTOR Caramel)
{
	if ((mouse.vecX - ConversionX(Caramel.vecX)) *(mouse.vecX - ConversionX(Caramel.vecX)) + (mouse.vecY - ConversionY(Caramel.vecY)) * (mouse.vecY - ConversionY(Caramel.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float SC_B::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xSC_BCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xSC_BCenter_;
	}
	else if (xPos > 0)
	{
		xSC_BCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xSC_BCenter_;
	}
	else if (xPos == 0)
	{
		xSC_BCenter_ = DispCenter_.vecX;
		return xSC_BCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float SC_B::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		ySC_BCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return ySC_BCenter_;
	}
	else if (yPos < 0)
	{
		ySC_BCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return ySC_BCenter_;
	}
	else if (yPos == 0)
	{
		ySC_BCenter_ = DispCenter_.vecY;
		return ySC_BCenter_;
	}
}
