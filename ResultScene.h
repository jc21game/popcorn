#pragma once
#include "Engine/GameObject.h"

//リザルトシーンを管理するクラス
class ResultScene : public GameObject
{
	int bgpict_;
	int hPict_[10];    //画像番号 
	int tenhPict_[10];
	int hundhPict_[10];
	int thoushPict_[10];
	int tenthoushPict_[10];
	int hund_thous_hPict_[10];
	int millionhPict_[10];
	int UnLockpict_;
	bool UnLock = false;
	int aScore_[7];

	Transform bgtrans_;
	Transform tentrans_;
	Transform hundtrans_;
	Transform thoustrans_;
	Transform tenthoustrans_;
	Transform hund_thous_trans_;
	Transform milliontrans_;
	Transform UnLocktrans_;

	int SleepCount = 0;

	bool Flow = true;

	char n = 0;
	char t = 0;
	char h = 0;
	char th = 0;
	char tt = 0;
	char ht = 0;
	char m = 0;

	int flag = 0;

	char randam;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//どのシーンのリザルトか
	void SceneJudg();
};