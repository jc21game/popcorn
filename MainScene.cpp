#include "MainScene.h"
#include "SettingUI.h"
#include "DifficultyLevel.h"
#include "BackGroundimage.h"
#include "FryingPan.h"
#include "Cup.h"
#include "Cone.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"



//Constructor
MainScene::MainScene(GameObject * parent)
	:GameObject(parent, "MainScene")
{

}

//Destructor
MainScene::~MainScene()
{
}

void MainScene::Initialize()
{
	//Instantiate<>(this);
	//Instantiate<>(this);

	//背景をMainシーンに表示
	Instantiate<BackGroundimage>(this);

	//フライパンをMainシーンに登場させる
	Instantiate<FryingPan>(this);

	Instantiate<Cup>(this);

	//コーンを表示させる
	Instantiate<Cone>(this);

}

void MainScene::Update()
{


}

void MainScene::Draw()
{

}

void MainScene::Release()
{
}
