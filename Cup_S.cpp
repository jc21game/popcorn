#include "Cup_S.h"
#include "PlayScene_S.h"
#include "Engine/Image.h"



//コンストラクタ
Cup_S::Cup_S(GameObject * parent)
	:GameObject(parent, "Cup_S")
{
	for (int i = 0; i < 5; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Cup_S::~Cup_S()
{
}

//初期化
void Cup_S::Initialize()
{
	//カップ画像のロード
	hPict_[0] = Image::Load("Cup.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("cuptwofive.png");
	assert(hPict_[1] >= 0);
	hPict_[2] = Image::Load("cupfifty.png");
	assert(hPict_[2] >= 0);
	hPict_[3] = Image::Load("cupsevenfive.png");
	assert(hPict_[3] >= 0);
	hPict_[4] = Image::Load("cuphundred.png");
	assert(hPict_[4] >= 0);
	transform_.position_.vecY = -0.2f;
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);

}

//更新
void Cup_S::Update()
{
	PlayScene_S* pPlayS = (PlayScene_S*)FindObject("PlayScene_S");
	if (pPlayS->GetCount() > 99)
	{
		flag_ = 4;
	}
	else if (pPlayS->GetCount() > 74)
	{
		flag_ = 3;
	}
	else if (pPlayS->GetCount() > 49)
	{
		flag_ = 2;
	}
	else if (pPlayS->GetCount() > 19)
	{
		flag_ = 1;
	}
}

//描画
void Cup_S::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Cup_S::Release()
{
}

