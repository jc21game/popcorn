#include "HomeScene.h"
#include "Back.h"
#include "DifficultyLevel.h"
#include "Scenery.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//コンストラクタ
HomeScene::HomeScene(GameObject * parent)
	:GameObject(parent, "HomeScene")
{

}

//デストラクタ
HomeScene::~HomeScene()
{
}

//初期化
void HomeScene::Initialize()
{
	Instantiate<Scenery>(this);
	Instantiate<Back>(this);
	Instantiate<DifficultyLevel>(this);
}

//更新
void HomeScene::Update()
{
	

}

//描画
void HomeScene::Draw()
{
	
}

//解放
void HomeScene::Release()
{
}
