#pragma once
#pragma once
#include "Engine/GameObject.h"

class SelectUI : public GameObject
{
private:
	int NhPict_;    //しお画像変数
	int EhPict_[2];    //バター画像変数


	Transform				Etransform_; //エクストラの画像位置

	XMVECTOR				MousePos_;   //マウスの位置座標を入れる変数

public:
	
	//コンストラクタ
	SelectUI(GameObject* parent);
	//デストラクタ
	~SelectUI();
	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//描画
	void Draw() override;
	//解放
	void Release() override;

	
};