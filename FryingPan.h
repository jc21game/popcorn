#pragma once
#include "Engine/GameObject.h"

//フライパンを管理するクラス
class FryingPan : public GameObject
{
	int hPict_;    //画像番号

public:
	//コンストラクタ
	FryingPan(GameObject* parent);

	//デストラクタ
	~FryingPan();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};