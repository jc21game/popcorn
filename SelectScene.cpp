#include "SelectScene.h"
#include "ResultScore.h"
#include "Scenery.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "SelectUI.h"


//コンストラクタ
SelectScene::SelectScene(GameObject * parent)
	:GameObject(parent, "SelectScene")
{

}

//デストラクタ
SelectScene::~SelectScene()
{
}

//初期化
void SelectScene::Initialize()
{
	ResultScore::resultScore_C = 0;
	ResultScore::resultScore_B = 0;
	ResultScore::resultScore_S = 0;
	ResultScore::MaxCombo_C = 0;
	ResultScore::MaxCombo_B = 0;
	ResultScore::MaxCombo_S = 0;
	ResultScore::CenterClick = 0;

	Instantiate<Scenery>(this);
	Instantiate<SelectUI>(this);
}

//更新
void SelectScene::Update()
{


}

//描画
void SelectScene::Draw()
{

}

//解放
void SelectScene::Release()
{
}
