#include "Endless.h"
#include "EndlessScene.h"
#include "Score_E.h"
#include "EndlessSpeed.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"
#include "Engine/Direct3D.h"
#include "Engine/Transform.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Endless::Endless(GameObject * parent)
	:GameObject(parent, "Endless"), hSound_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Endless::~Endless()
{
}

//初期化
void Endless::Initialize()
{
	EndlessScene* pEnd = (EndlessScene*)FindObject("EndlessScene");
	hPict_[0] = Image::Load("Corn.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("PopCorn.png");
	assert(hPict_[1] >= 0);
	//サウンドデータのロード
	hSound_ = Audio::Load("sound.wav");
	assert(hSound_ >= 0);

	transform_.position_.vecY = 1.0f;
	//コーンが降ってくる位置をランダムにする
	switch (pEnd->PopCount_)
	{
	case 3:
		switch (pEnd->line_)
		{
			case 1:rNumber_ = rand() % 3 - 9; break;
			case 2:rNumber_ = rand() % 6 - 3; break;
			case 3:rNumber_ = rand() % 5 + 5;
		}
	case 4:
		switch (pEnd->line_)
		{
		case 1:rNumber_ = rand() % 2 - 9; break;
		case 2:rNumber_ = rand() % 4 - 6; break;
		case 3:rNumber_ = rand() % 3 + 2; break;
		case 4:rNumber_ = rand() % 3 + 6;
		}
	case 5:
		switch (pEnd->line_)
		{
		case 1:rNumber_ = rand() % 2 - 9; break;
		case 2:rNumber_ = rand() % 3 - 6; break;
		case 3:rNumber_ = rand() % 3 - 2; break;
		case 4:rNumber_ = rand() % 3 + 2; break;
		case 5:rNumber_ = rand() % 3 + 6;
		}
	
	}

	transform_.position_.vecX = rNumber_ * 0.1f;
	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);


}

//更新
void Endless::Update()
{
	if (flag)
	{
		if (++Count_ > 10)
		{
			KillMe();
		}
	}
	transform_.position_.vecY -= EndlessSpeed::CornSpeed;  //コーンを落とす
	EndlessScene* pEnd = (EndlessScene*)FindObject("EndlessScene");
	Score_E* pScoreE = (Score_E*)FindObject("Score_E");

	//マウス(左)が押されたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの座標を取得
		MousePos_ = Input::GetMousePosition();
		//カップよりも上の場所で押されているか(Judge関数はReleaseの下に記載しています)
		if (MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 245 && Judge(MousePos_, transform_.position_))
		{
			Audio::Play(hSound_);
			pEnd->CountUp();
			flag_ = 1;

			if (pEnd->GetComboCount() < 20)
			{
				pScoreE->ScoreUp(1);
			}
			else if (pEnd->GetComboCount() >= 20 && pEnd->GetComboCount() < 50)
			{
				pScoreE->ScoreUp(2);
			}

			else if (pEnd->GetComboCount() >= 50)
			{
				pScoreE->ScoreUp(3);
			}
			flag = true;

		}
	}

	//弾がある程度、下に行ったら消える
	if (transform_.position_.vecY < -1.0f)
	{
		pEnd->DeadCountUp();
		pEnd->ZeroCount();
		KillMe();
	}
}

//描画
void Endless::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Endless::Release()
{
}

//マウスとコーンの当たり判定
bool Endless::Judge(XMVECTOR mouse, XMVECTOR Endless)
{
	if ((mouse.vecX - ConversionX(Endless.vecX)) *(mouse.vecX - ConversionX(Endless.vecX)) + (mouse.vecY - ConversionY(Endless.vecY)) * (mouse.vecY - ConversionY(Endless.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float Endless::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xEndlessCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xEndlessCenter_;
	}
	else if (xPos > 0)
	{
		xEndlessCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xEndlessCenter_;
	}
	else if (xPos == 0)
	{
		xEndlessCenter_ = DispCenter_.vecX;
		return xEndlessCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float Endless::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		yEndlessCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return yEndlessCenter_;
	}
	else if (yPos < 0)
	{
		yEndlessCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return yEndlessCenter_;
	}
	else if (yPos == 0)
	{
		yEndlessCenter_ = DispCenter_.vecY;
		return yEndlessCenter_;
	}
}
