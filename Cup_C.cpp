#include "Cup_C.h"
#include "PlayScene_C.h"
#include "Engine/Image.h"



//コンストラクタ
Cup_C::Cup_C(GameObject * parent)
	:GameObject(parent, "Cup_C")
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Cup_C::~Cup_C()
{
}

//初期化
void Cup_C::Initialize()
{
	//カップ画像のロード
	hPict_[0] = Image::Load("Cup.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("cuptwofive.png");
	assert(hPict_[1] >= 0);
	transform_.position_.vecY = -0.2f;
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);
	
}

//更新
void Cup_C::Update()
{
	PlayScene_C* pPlayC = (PlayScene_C*)FindObject("PlayScene_C");
	if (pPlayC->GetCount() > 19) flag_ = 1;
}

//描画
void Cup_C::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Cup_C::Release()
{
}

