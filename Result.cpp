#include "Result.h"
#include "ResultScene.h"
#include "ReleaseEffect.h"
#include "LockManager.h"
#include "MenCharacter.h"
#include "WoMenCharacter.h"
#include "Scenery.h"
#include "Engine/SceneManager.h"


//コンストラクタ
Result::Result(GameObject * parent)
	:GameObject(parent, "Result")
{

}

//デストラクタ
Result::~Result()
{
}

//初期化
void Result::Initialize()
{
	Instantiate<Scenery>(this);
	Instantiate<ResultScene>(this);
	Instantiate<MenCharacter>(this);
	Instantiate<WoMenCharacter>(this);
}

//更新
void Result::Update()
{
	if (LockManager::disLock)
	{
		Instantiate<ReleaseEffect>(this);
	}
}

//描画
void Result::Draw()
{

}

//解放
void Result::Release()
{
}
