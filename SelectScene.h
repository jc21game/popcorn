#pragma once
#include "Engine/GameObject.h"

class SelectScene : public GameObject
{
private:

public:
	//コンストラクタ
	SelectScene(GameObject* parent);

	//デストラクタ
	~SelectScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;
};
