#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class WoMenCharacter : public GameObject
{
	int hPict_[3];

public:
	//コンストラクタ
	WoMenCharacter(GameObject* parent);

	//デストラクタ
	~WoMenCharacter();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};