#pragma once
#include "Engine/GameObject.h"

//BackGroundimageを管理するクラス
class Back : public GameObject
{
	int hPict_;    //画像番号

	XMVECTOR MousePos_;
	XMVECTOR DispCenter_;
	float    xBackCenter_; //コーンの現在位置X座標を入れる変数
	float    yBackCenter_;

public:
	//コンストラクタ
	Back(GameObject* parent);

	//デストラクタ
	~Back();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	bool Judge(XMVECTOR mouse, XMVECTOR Caramel);

	//画像をスクリーン座標に変換
	float ConversionX(float Pos);
	float ConversionY(float Pos);
};