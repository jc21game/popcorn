#pragma once
#include "Engine/GameObject.h"

class EndlessScene : public GameObject
{
private:
	int CupCounter_ = 0;
	int ComboCounter_ = 0;
	int LoopCounter_ = 0;
	int Count_ = 0;
	bool flag_ = false;
	char DeadFlag_ = 0;
	char LoopCount_ = 0;
	int pRandom = 0;
public:
	char PopCount_ = 3;
	char line_ = 1;

	//コンストラクタ
	EndlessScene(GameObject* parent);

	//デストラクタ
	~EndlessScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	void CountUp();

	int GetCount();

	int GetComboCount();

	void ZeroCount();

	void DeadCountUp();

};

