#include "SelectUI.h"
#include "LockManager.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"


//コンストラクタ
SelectUI::SelectUI(GameObject * parent)
	:GameObject(parent, "SelectUI"), NhPict_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		EhPict_[i] = -1;
	}
}

//デストラクタ
SelectUI::~SelectUI()
{
}

//初期化
void SelectUI::Initialize()
{
	//ノーマル画像のロード
	NhPict_ = Image::Load("Normal.png");
	assert(NhPict_ >= 0);
	transform_.position_.vecY = 0.5f;
	//エクストラ画像のロード
	EhPict_[0] = Image::Load("ExtraLook.png");
	assert(EhPict_[0] >= 0);
	EhPict_[1] = Image::Load("Extra.png");
	assert(EhPict_[1] >= 0);
	Etransform_.position_.vecY = -0.5f;
	
	
}

//更新
void SelectUI::Update()
{
	//クリックしたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの情報取得
		MousePos_ = Input::GetMousePosition();
		//画像とマウスの座標を比べる
		//ノーマル
		if (MousePos_.vecX > (Direct3D::screenWidth_ / 2) - 512 && MousePos_.vecX < (Direct3D::screenWidth_ / 2) + 512 && MousePos_.vecY >(Direct3D::screenHeight_ / 2 * 0.5) - 128 && MousePos_.vecY < (Direct3D::screenHeight_ / 2 * 0.5) + 128)
		{
			//シーンの切り替え(プレイシーンへ)
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_HOME);
		}
		//エンドレス
		if (MousePos_.vecX > (Direct3D::screenWidth_ / 2) - 512 && MousePos_.vecX < (Direct3D::screenWidth_ / 2) + 512 && MousePos_.vecY >(Direct3D::screenHeight_  / 2 + (Direct3D::screenHeight_ / 2 - Direct3D::screenHeight_ / 2 * 0.5)) - 128 && MousePos_.vecY < (Direct3D::screenHeight_ / 2 + (Direct3D::screenHeight_ / 2 - Direct3D::screenHeight_ / 2 * 0.5) + 128))
		{
			if (LockManager::Lock)
			{
				//シーンの切り替え(プレイシーンへ)
				SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
				pSceneManager->ChangeScene(SCENE_ID_END);
				LockManager::Lock = false;
			}

		}
	}
}

//描画
void SelectUI::Draw()
{
	//ノーマルの描画
	Image::SetTransform(NhPict_, transform_);
	Image::Draw(NhPict_);
	//エクストラの描画
	if (LockManager::Lock)
	{
		Image::SetTransform(EhPict_[1], Etransform_);
		Image::Draw(EhPict_[1]);
	}
	else
	{
		Image::SetTransform(EhPict_[0], Etransform_);
		Image::Draw(EhPict_[0]);
	}
}

//解放
void SelectUI::Release()
{
}


