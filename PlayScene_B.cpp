#include "PlayScene_B.h"
#include "DifficultyLevel.h"
#include "BackGroundimage.h"
#include "FryingPan.h"
#include "Cup_B.h"
#include "Butter.h"
#include "SC_B.h"
#include "Combo_B.h"
#include "Score_B.h"
#include "Timer.h"
#include "ResultScore.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
PlayScene_B::PlayScene_B(GameObject * parent)
	:GameObject(parent, "PlayScene_B")
{

}

//デストラクタ
PlayScene_B::~PlayScene_B()
{
}

//初期化
void PlayScene_B::Initialize()
{
	ResultScore::Popcorn = 3;

	//背景を表示させる
	Instantiate<BackGroundimage>(this);

	//フライパンを表示させる
	Instantiate<FryingPan>(this);

	//カップを表示させる
	Instantiate<Cup_B>(this);

	//コンボを表示させる
	Instantiate<Combo_B>(this);

	//スコアを表示させる
	Instantiate<Score_B>(this);


	//コーンを表示させる
	for (int i = 0; i < 4; i++)
	{
		Instantiate<Butter>(this);
		line_ += 1;
	}

	//タイマーを表示させる
	Instantiate<Timer>(this);

}

//更新
void PlayScene_B::Update()
{
	if (FindChildObject("Butter") == nullptr)
	{
		ResultScore::Popcorn = 3;

		if (FindChildObject("Timer") == nullptr)
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_RESULT);
		}
		else
		{
			if (LoopCounter_ % 6 == 0)Instantiate<SC_B>(this);
			line_ = 1;
			for (int i = 0; i < 4; i++)
			{
				Instantiate<Butter>(this);
				line_ += 1;
			}
			LoopCounter_ += 1;		
			clickflag_ = true;
		}
	}

}

//描画
void PlayScene_B::Draw()
{

}

//解放
void PlayScene_B::Release()
{
}

void PlayScene_B::CountUp()
{
	CupCounter_ = CupCounter_ + 1;
	ComboCounter_ = ComboCounter_ + 1;
	ResultScore::MaxCombo_B = ComboCounter_;
}

int PlayScene_B::GetCount()
{
	return CupCounter_;
}

int PlayScene_B::GetComboCount()
{
	return ComboCounter_;
}

void PlayScene_B::ZeroCount()
{
	if (ResultScore::MaxCombo_B == 0)
	{
		ResultScore::MaxCombo_B = ComboCounter_;
	}
	else
	{
		if (ResultScore::MaxCombo_B < ComboCounter_)
		{
			ResultScore::MaxCombo_B = ComboCounter_;
		}
	}
	ComboCounter_ = 0;
}
