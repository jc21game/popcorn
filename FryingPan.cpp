#include "FryingPan.h"
#include "Engine/Image.h"

//コンストラクタ
FryingPan::FryingPan(GameObject * parent)
	:GameObject(parent, "FryingPan"), hPict_(-1)
{
}

//デストラクタ
FryingPan::~FryingPan()
{
}

//初期化
void FryingPan::Initialize()
{

	//フライパン画像データのロード
	hPict_ = Image::Load("fryingPan.png");
	assert(hPict_ >= 0);

	//フライパンの位置
	transform_.position_ = XMVectorSet(-0.9, 0.55, 0, 0);
	//フライパンの拡大
	transform_.scale_ = XMVectorSet(0.6f, 0.6f, 1.0f, 0.0f);


}

//更新
void FryingPan::Update()
{
}

//描画
void FryingPan::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void FryingPan::Release()
{
}