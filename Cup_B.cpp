#include "Cup_B.h"
#include "PlayScene_B.h"
#include "Engine/Image.h"



//コンストラクタ
Cup_B::Cup_B(GameObject * parent)
	:GameObject(parent, "Cup_B")
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Cup_B::~Cup_B()
{
}

//初期化
void Cup_B::Initialize()
{
	//カップ画像のロード
	hPict_[0] = Image::Load("Cup.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("cuptwofive.png");
	assert(hPict_[1] >= 0);
	hPict_[2] = Image::Load("cupfifty.png");
	assert(hPict_[2] >= 0);
	transform_.position_.vecY = -0.2f;
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);

}

//更新
void Cup_B::Update()
{
	PlayScene_B* pPlayB = (PlayScene_B*)FindObject("PlayScene_B");
	if (pPlayB->GetCount() > 49) flag_ = 2;
	else if (pPlayB->GetCount() > 19) flag_ = 1;
}

//描画
void Cup_B::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Cup_B::Release()
{
}

