#include "BackGroundimage.h"
#include "Engine/Image.h"

//コンストラクタ
BackGroundimage::BackGroundimage(GameObject * parent)
	:GameObject(parent, "BackGroundimage"), hPict_(-1)
{
}

//デストラクタ
BackGroundimage::~BackGroundimage()
{
}

//初期化
void BackGroundimage::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("backGroundkai.png");
	assert(hPict_ >= 0);

	//位置、拡大縮小ともにデフォルトの状態です
	transform_.position_ = XMVectorSet(0, 0, 0, 0);				//位置
	transform_.scale_ = XMVectorSet(3.15f, 3.15f, 1.0f, 0.0f);	//拡大・縮小
}

//更新
void BackGroundimage::Update()
{

}

//描画
void BackGroundimage::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void BackGroundimage::Release()
{
}