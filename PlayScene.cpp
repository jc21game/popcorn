#include "PlayScene.h"
#include "DifficultyLevel.h"
#include "BackGroundimage.h"
#include "FryingPan.h"
#include "Cup.h"
#include "Cone.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"



//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	:GameObject(parent, "PlayScene")
{

}

//デストラクタ
PlayScene::~PlayScene()
{
}

//初期化
void PlayScene::Initialize()
{

	//背景を表示させる
	Instantiate<BackGroundimage>(this);

	//フライパンを表示させる
	Instantiate<FryingPan>(this);

	//カップを表示させる
	Instantiate<Cup>(this);

	//コーンを表示させる
	for (int i = 0; i < 200; i++)
	{
		Instantiate<Cone>(this);
	}

}

//更新
void PlayScene::Update()
{
	if (FindChildObject("Cone") == nullptr)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_HOME);
	}

}

//描画
void PlayScene::Draw()
{

}

//解放
void PlayScene::Release()
{
}
