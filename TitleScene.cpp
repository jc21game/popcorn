#include "TitleScene.h"
#include "Engine/Image.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene"), hPict_(-1)
{
}

//デストラクタ
TitleScene::~TitleScene()
{
}

//初期化
void TitleScene::Initialize()
{
	//タイトルロゴ表示
	hPict_ = Image::Load("Title.png");
	assert(hPict_ >= 0);
	transform_.scale_ = XMVectorSet(3.13f, 3.13f, 0, 0);
}

//更新
void TitleScene::Update()
{
	//マウス(右)を押したらスタート
	if (Input::IsMouseButtonDown(0))
	{
		//シーンの切り替え->ホームシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_SELECT);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TitleScene::Release()
{
}
