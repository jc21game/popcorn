#include "Back.h"
#include "SelectScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Direct3D.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Back::Back(GameObject * parent)
	:GameObject(parent, "Back"), hPict_(-1)
{
}

//デストラクタ
Back::~Back()
{
}

//初期化
void Back::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("back.png");
	assert(hPict_ >= 0);

	//位置、拡大縮小ともにデフォルトの状態です
	transform_.position_ = XMVectorSet(-0.9f, 0.9f, 0, 0);				//位置

	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);
}

//更新
void Back::Update()
{
	SelectScene* pSelect = (SelectScene*)FindObject("SelectScene");
	if (Input::IsMouseButtonDown(0))
	{
		MousePos_ = Input::GetMousePosition();
		if (Judge(MousePos_, transform_.position_))
		{
			//シーンの切り替え->ホームシーンへ
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_SELECT);
		}
	}
}

//描画
void Back::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Back::Release()
{
}

//マウスとコーンの当たり判定
bool Back::Judge(XMVECTOR mouse, XMVECTOR Back)
{
	if ((mouse.vecX - ConversionX(Back.vecX)) *(mouse.vecX - ConversionX(Back.vecX)) + (mouse.vecY - ConversionY(Back.vecY)) * (mouse.vecY - ConversionY(Back.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float Back::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xBackCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xBackCenter_;
	}
	else if (xPos > 0)
	{
		xBackCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xBackCenter_;
	}
	else if (xPos == 0)
	{
		xBackCenter_ = DispCenter_.vecX;
		return xBackCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float Back::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		yBackCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return yBackCenter_;
	}
	else if (yPos < 0)
	{
		yBackCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return yBackCenter_;
	}
	else if (yPos == 0)
	{
		yBackCenter_ = DispCenter_.vecY;
		return yBackCenter_;
	}
}
