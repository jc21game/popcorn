#include "WoMenCharacter.h"
#include "Engine/Image.h"


//コンストラクタ
WoMenCharacter::WoMenCharacter(GameObject * parent)
	:GameObject(parent, "WoMenCharacter")
{
	for (int i = 0; i < 3; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
WoMenCharacter::~WoMenCharacter()
{
}

//初期化
void WoMenCharacter::Initialize()
{
	hPict_[0] = Image::Load("WCorn_Stand.png");
	hPict_[1] = Image::Load("WCorn_Think.png");
	hPict_[2] = Image::Load("WCorn_Step.png");
	assert(hPict_[0] >= 0);
	assert(hPict_[1] >= 0);
	assert(hPict_[2] >= 0);

	transform_.position_ = XMVectorSet(-0.7f, 0, 0, 0);
}

//更新
void WoMenCharacter::Update()
{


}

//描画
void WoMenCharacter::Draw()
{
	Image::SetTransform(hPict_[0], transform_);
	Image::Draw(hPict_[0]);
}

//開放
void WoMenCharacter::Release()
{
}

