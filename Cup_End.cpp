#include "Cup_End.h"
#include "EndlessScene.h"
#include "Engine/Image.h"



//コンストラクタ
Cup_End::Cup_End(GameObject * parent)
	:GameObject(parent, "Cup_End")
{
	for (int i = 0; i < 5; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Cup_End::~Cup_End()
{
}

//初期化
void Cup_End::Initialize()
{
	//カップ画像のロード
	hPict_[0] = Image::Load("Cup.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("cuptwofive.png");
	assert(hPict_[1] >= 0);
	hPict_[2] = Image::Load("cupfifty.png");
	assert(hPict_[2] >= 0);
	hPict_[3] = Image::Load("cupsevenfive.png");
	assert(hPict_[3] >= 0);
	hPict_[4] = Image::Load("cuphundred.png");
	assert(hPict_[4] >= 0);
	transform_.position_.vecY = -0.2f;
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);

}

//更新
void Cup_End::Update()
{
	EndlessScene* pPlayS = (EndlessScene*)FindObject("EndlessScene");
	if (pPlayS->GetCount() > 99)
	{
		flag_ = 4;
	}
	else if (pPlayS->GetCount() > 74)
	{
		flag_ = 3;
	}
	else if (pPlayS->GetCount() > 49)
	{
		flag_ = 2;
	}
	else if (pPlayS->GetCount() > 19)
	{
		flag_ = 1;
	}
}

//描画
void Cup_End::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Cup_End::Release()
{
}

