#include "MenCharacter.h"
#include "Engine/Image.h"


//コンストラクタ
MenCharacter::MenCharacter(GameObject * parent)
	:GameObject(parent, "MenCharacter")
{
	for (int i = 0; i < 3; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
MenCharacter::~MenCharacter()
{
}

//初期化
void MenCharacter::Initialize()
{
	hPict_[0] = Image::Load("MCorn_Face.png");
	hPict_[1] = Image::Load("MCorn_Yeah.png");
	hPict_[2] = Image::Load("MCorn_Yeah.png"); 
	assert(hPict_[0] >= 0);
	assert(hPict_[1] >= 0);
	assert(hPict_[2] >= 0);

	transform_.position_ = XMVectorSet(0.7f, 0, 0, 0);
}

//更新
void MenCharacter::Update()
{
	

}

//描画
void MenCharacter::Draw()
{
	Image::SetTransform(hPict_[0], transform_);
	Image::Draw(hPict_[0]);
}

//開放
void MenCharacter::Release()
{
}

