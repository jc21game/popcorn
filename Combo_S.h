#pragma once
#include "Engine/GameObject.h"

//コンボを管理するクラス
class Combo_S : public GameObject
{
	int hPict_[10];    //画像番号 
	int tenhPict_[10];
	int hundhPict_[10];

	int ComboCounter_ = 0;

	int aScore_[2];
	Transform tentrans_;
	Transform handtrans_;

	char n = 0;
	char t = 0;
	char h = 0;



public:
	//コンストラクタ
	Combo_S(GameObject* parent);

	//デストラクタ
	~Combo_S();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};