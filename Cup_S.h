#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class Cup_S : public GameObject
{
	int hPict_[5];    //画像番号 
	char flag_ = 0;

public:
	//コンストラクタ
	Cup_S(GameObject* parent);

	//デストラクタ
	~Cup_S();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};