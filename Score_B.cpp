#include "Score_B.h"
#include "ResultScore.h"
#include "Engine/Image.h"

Score_B::Score_B(GameObject * parent) :
	GameObject(parent, "Score_B"), fPict_(-1)
{
	for (int i = 0; i < 10; i++)
	{
		hPict_[i] = -1;
		tenhPict_[i] = -1;
		hundhPict_[i] = -1;
		thoushPict_[i] = -1;
		tenthoushPict_[i] = -1;
		hund_thous_hPict_[i] = -1;
		millionhPict_[i] = -1;
	}

	for (int i = 0; i < 7; i++)
	{
		aScore_[i] = 0;
	}
}

Score_B::~Score_B()
{
}

void Score_B::Initialize()
{
	fPict_ = Image::Load("Score.png");
	assert(fPict_ >= 0);
	for (int i = 0; i < 10; i++)
	{
		switch (i)
		{
		case 0:
			hPict_[i] = Image::Load("zero.png");
			tenhPict_[i] = Image::Load("zero.png");
			hundhPict_[i] = Image::Load("zero.png");
			thoushPict_[i] = Image::Load("zero.png");
			tenthoushPict_[i] = Image::Load("zero.png");
			hund_thous_hPict_[i] = Image::Load("zero.png");
			millionhPict_[i] = Image::Load("zero.png");
			break;
		case 1:
			hPict_[i] = Image::Load("one.png");
			tenhPict_[i] = Image::Load("one.png");
			hundhPict_[i] = Image::Load("one.png");
			thoushPict_[i] = Image::Load("one.png");
			tenthoushPict_[i] = Image::Load("one.png");
			hund_thous_hPict_[i] = Image::Load("one.png");
			millionhPict_[i] = Image::Load("one.png");
			break;
		case 2:
			hPict_[i] = Image::Load("two.png");
			tenhPict_[i] = Image::Load("two.png");
			hundhPict_[i] = Image::Load("two.png");
			thoushPict_[i] = Image::Load("two.png");
			tenthoushPict_[i] = Image::Load("two.png");
			hund_thous_hPict_[i] = Image::Load("two.png");
			millionhPict_[i] = Image::Load("two.png");
			break;
		case 3:
			hPict_[i] = Image::Load("three.png");
			tenhPict_[i] = Image::Load("three.png");
			hundhPict_[i] = Image::Load("three.png");
			thoushPict_[i] = Image::Load("three.png");
			tenthoushPict_[i] = Image::Load("three.png");
			hund_thous_hPict_[i] = Image::Load("three.png");
			millionhPict_[i] = Image::Load("three.png");
			break;
		case 4:
			hPict_[i] = Image::Load("four.png");
			tenhPict_[i] = Image::Load("four.png");
			hundhPict_[i] = Image::Load("four.png");
			thoushPict_[i] = Image::Load("four.png");
			tenthoushPict_[i] = Image::Load("four.png");
			hund_thous_hPict_[i] = Image::Load("four.png");
			millionhPict_[i] = Image::Load("four.png");
			break;
		case 5:
			hPict_[i] = Image::Load("five.png");
			tenhPict_[i] = Image::Load("five.png");
			hundhPict_[i] = Image::Load("five.png");
			thoushPict_[i] = Image::Load("five.png");
			tenthoushPict_[i] = Image::Load("five.png");
			hund_thous_hPict_[i] = Image::Load("five.png");
			millionhPict_[i] = Image::Load("five.png");
			break;
		case 6:
			hPict_[i] = Image::Load("six.png");
			tenhPict_[i] = Image::Load("six.png");
			hundhPict_[i] = Image::Load("six.png");
			thoushPict_[i] = Image::Load("six.png");
			tenthoushPict_[i] = Image::Load("six.png");
			hund_thous_hPict_[i] = Image::Load("six.png");
			millionhPict_[i] = Image::Load("six.png");
			break;
		case 7:
			hPict_[i] = Image::Load("seven.png");
			tenhPict_[i] = Image::Load("seven.png");
			hundhPict_[i] = Image::Load("seven.png");
			thoushPict_[i] = Image::Load("seven.png");
			tenthoushPict_[i] = Image::Load("seven.png");
			hund_thous_hPict_[i] = Image::Load("seven.png");
			millionhPict_[i] = Image::Load("seven.png");
			break;
		case 8:
			hPict_[i] = Image::Load("eight.png");
			tenhPict_[i] = Image::Load("eight.png");
			hundhPict_[i] = Image::Load("eight.png");
			thoushPict_[i] = Image::Load("eight.png");
			tenthoushPict_[i] = Image::Load("eight.png");
			hund_thous_hPict_[i] = Image::Load("eight.png");
			millionhPict_[i] = Image::Load("eight.png");
			break;
		case 9:
			hPict_[i] = Image::Load("nine.png");
			tenhPict_[i] = Image::Load("nine.png");
			hundhPict_[i] = Image::Load("nine.png");
			thoushPict_[i] = Image::Load("nine.png");
			tenthoushPict_[i] = Image::Load("nine.png");
			hund_thous_hPict_[i] = Image::Load("nine.png");
			millionhPict_[i] = Image::Load("nine.png");
		}
		assert(hPict_[i] >= 0);
		assert(tenhPict_[i] >= 0);
		assert(hundhPict_[i] >= 0);
		assert(thoushPict_[i] >= 0);
		assert(tenthoushPict_[i] >= 0);
		assert(hund_thous_hPict_[i] >= 0);
		assert(millionhPict_[i] >= 0);
	}

	ftrans_.position_ = XMVectorSet(0.75f, 0.9f, 0, 0);
	ftrans_.scale_ = XMVectorSet(0.6f, 0.35f, 0, 1);
	transform_.position_ = XMVectorSet(0.9f, 0.9f, 0, 0);
	transform_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	tentrans_.position_ = XMVectorSet(0.85f, 0.9f, 0, 0);
	tentrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	hundtrans_.position_ = XMVectorSet(0.8f, 0.9f, 0, 0);
	hundtrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 0);
	thoustrans_.position_ = XMVectorSet(0.75f, 0.9f, 0, 0);
	thoustrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	tenthoustrans_.position_ = XMVectorSet(0.7f, 0.9f, 0, 0);
	tenthoustrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	hund_thous_trans_.position_ = XMVectorSet(0.65f, 0.9f, 0, 0);
	hund_thous_trans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	milliontrans_.position_ = XMVectorSet(0.6f, 0.9f, 0, 0);
	milliontrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
}

void Score_B::Update()
{
	aScore_[0] = ResultScore::resultScore_B / 1000000;
	aScore_[1] = ResultScore::resultScore_B % 1000000 / 1000000;
	aScore_[2] = ResultScore::resultScore_B % 100000 / 10000;
	aScore_[3] = ResultScore::resultScore_B % 10000 / 1000;
	aScore_[4] = ResultScore::resultScore_B % 1000 / 100;
	aScore_[5] = ResultScore::resultScore_B % 100 / 10;
	aScore_[6] = ResultScore::resultScore_B % 10;

	if (ResultScore::resultScore_B == 0)
	{
		n = 0;
		t = 0;
		h = 0;
		th = 0;
		tt = 0;
		ht = 0;
		m = 0;
	}

	switch (aScore_[6])
	{
	case 0:n = 0; break;
	case 1:n = 1; break;
	case 2:n = 2; break;
	case 3:n = 3; break;
	case 4:n = 4; break;
	case 5:n = 5; break;
	case 6:n = 6; break;
	case 7:n = 7; break;
	case 8:n = 8; break;
	case 9:n = 9;
	}

	switch (aScore_[5])
	{
	case 0:t = 0; break;
	case 1:t = 1; break;
	case 2:t = 2; break;
	case 3:t = 3; break;
	case 4:t = 4; break;
	case 5:t = 5; break;
	case 6:t = 6; break;
	case 7:t = 7; break;
	case 8:t = 8; break;
	case 9:t = 9;
	}

	switch (aScore_[4])
	{
	case 0:h = 0; break;
	case 1:h = 1; break;
	case 2:h = 2; break;
	case 3:h = 3; break;
	case 4:h = 4; break;
	case 5:h = 5; break;
	case 6:h = 6; break;
	case 7:h = 7; break;
	case 8:h = 8; break;
	case 9:h = 9;
	}

	switch (aScore_[3])
	{
	case 0:th = 0; break;
	case 1:th = 1; break;
	case 2:th = 2; break;
	case 3:th = 3; break;
	case 4:th = 4; break;
	case 5:th = 5; break;
	case 6:th = 6; break;
	case 7:th = 7; break;
	case 8:th = 8; break;
	case 9:th = 9;
	}

	switch (aScore_[2])
	{
	case 0:tt = 0; break;
	case 1:tt = 1; break;
	case 2:tt = 2; break;
	case 3:tt = 3; break;
	case 4:tt = 4; break;
	case 5:tt = 5; break;
	case 6:tt = 6; break;
	case 7:tt = 7; break;
	case 8:tt = 8; break;
	case 9:tt = 9;
	}

	switch (aScore_[1])
	{
	case 0:ht = 0; break;
	case 1:ht = 1; break;
	case 2:ht = 2; break;
	case 3:ht = 3; break;
	case 4:ht = 4; break;
	case 5:ht = 5; break;
	case 6:ht = 6; break;
	case 7:ht = 7; break;
	case 8:ht = 8; break;
	case 9:ht = 9;
	}

	switch (aScore_[0])
	{
	case 0:m = 0; break;
	case 1:m = 1; break;
	case 2:m = 2; break;
	case 3:m = 3; break;
	case 4:m = 4; break;
	case 5:m = 5; break;
	case 6:m = 6; break;
	case 7:m = 7; break;
	case 8:m = 8; break;
	case 9:m = 9;
	}
}

void Score_B::Draw()
{
	Image::SetTransform(fPict_, ftrans_);
	Image::Draw(fPict_);
	Image::SetTransform(hPict_[n], transform_);
	Image::Draw(hPict_[n]);
	Image::SetTransform(tenhPict_[t], tentrans_);
	Image::Draw(tenhPict_[t]);
	Image::SetTransform(hundhPict_[h], hundtrans_);
	Image::Draw(hundhPict_[h]);
	Image::SetTransform(thoushPict_[th], thoustrans_);
	Image::Draw(thoushPict_[th]);
	Image::SetTransform(tenthoushPict_[tt], tenthoustrans_);
	Image::Draw(tenthoushPict_[tt]);
	Image::SetTransform(hund_thous_hPict_[ht], hund_thous_trans_);
	Image::Draw(hund_thous_hPict_[ht]);
	Image::SetTransform(millionhPict_[m], milliontrans_);
	Image::Draw(millionhPict_[m]);
}

void Score_B::Release()
{
}

void Score_B::ScoreUp(int num)
{
	ResultScore::resultScore_B += num;
}
