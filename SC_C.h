#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class SC_C : public GameObject
{
	int hPict_[2];    //画像番号 
	int hSound_;
	int rNumber_;  // ランダム値
	int rPos_;
	float CornSpeed_ = 0.01f;    //コーンの落ちる速度

	bool flag = false;
	char flag_ = 0;

	int Count_; // 画像が変化してから消えるまでのカウント変数



	XMVECTOR MousePos_;   //マウスの座標を入れる変数
	XMVECTOR DispCenter_;  //画面の中心座標
	float    xSC_CCenter_; //コーンの現在位置X座標を入れる変数
	float    ySC_CCenter_;

public:
	//コンストラクタ
	SC_C(GameObject* parent);

	//デストラクタ
	~SC_C();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	bool Judge(XMVECTOR mouse, XMVECTOR SC_C);

	//画像をスクリーン座標に変換
	float ConversionX(float Pos);
	float ConversionY(float Pos);

};