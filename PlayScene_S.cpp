#include "PlayScene_S.h"
#include "DifficultyLevel.h"
#include "BackGroundimage.h"
#include "FryingPan.h"
#include "Cup_S.h"
#include "Salt.h"
#include "SC_S.h"
#include "Combo_S.h"
#include "Score_S.h"
#include "Timer.h"
#include "ResultScore.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
PlayScene_S::PlayScene_S(GameObject * parent)
	:GameObject(parent, "PlayScene_S")
{

}

//デストラクタ
PlayScene_S::~PlayScene_S()
{
}

//初期化
void PlayScene_S::Initialize()
{

	ResultScore::Popcorn = 4;

	//背景を表示させる
	Instantiate<BackGroundimage>(this);

	//フライパンを表示させる
	Instantiate<FryingPan>(this);

	//カップを表示させる
	Instantiate<Cup_S>(this);

	//コンボを表示させる
	Instantiate<Combo_S>(this);

	//スコアを表示させる
	Instantiate<Score_S>(this);


	//コーンを表示させる
	for (int i = 0; i < 5; i++)
	{
		Instantiate<Salt>(this);
		line_ += 1;
	}

	//タイマーを表示させる
	Instantiate<Timer>(this);

}

//更新
void PlayScene_S::Update()
{
	if (FindChildObject("Salt") == nullptr)
	{
		ResultScore::Popcorn = 4;
		if (FindChildObject("Timer") == nullptr)
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_RESULT);
		}
		else
		{
			if (LoopCounter_ % 7 == 0)Instantiate<SC_S>(this);
			line_ = 1;
			for (int i = 0; i < 5; i++)
			{
				Instantiate<Salt>(this);
				line_ += 1;
			}
			LoopCounter_ += 1;
			clickflag_ = true;
		}
	}

}

//描画
void PlayScene_S::Draw()
{

}

//解放
void PlayScene_S::Release()
{
}

void PlayScene_S::CountUp()
{
	CupCounter_ = CupCounter_ + 1;
	ComboCounter_ = ComboCounter_ + 1;
	ResultScore::MaxCombo_S = ComboCounter_;
}

int PlayScene_S::GetCount()
{
	return CupCounter_;
}

int PlayScene_S::GetComboCount()
{
	return ComboCounter_;
}

void PlayScene_S::ZeroCount()
{
	if (ResultScore::MaxCombo_S == 0)
	{
		ResultScore::MaxCombo_S = ComboCounter_;
	}
	else
	{
		if (ResultScore::MaxCombo_S < ComboCounter_)
		{
			ResultScore::MaxCombo_S = ComboCounter_;
		}
	}
	ComboCounter_ = 0;
}
