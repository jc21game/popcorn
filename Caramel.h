#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class Caramel : public GameObject
{
	int hPict_[2];    //画像番号 
	int hSound_;
	int rNumber_;  // ランダム値

	float CornSpeed_ = 0.003f;    //コーンの落ちる速度

	bool flag = false;
	char flag_ = 0;

	int Count_; // 画像が変化してから消えるまでのカウント変数

	

	XMVECTOR MousePos_;   //マウスの座標を入れる変数
	XMVECTOR DispCenter_ ;  //画面の中心座標
	float    xCaramelCenter_; //コーンの現在位置X座標を入れる変数
	float    yCaramelCenter_;

public:
	//コンストラクタ
	Caramel(GameObject* parent);

	//デストラクタ
	~Caramel();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	
	bool Judge(XMVECTOR mouse,XMVECTOR Caramel);

	//画像をスクリーン座標に変換
    float ConversionX(float Pos);
	float ConversionY(float Pos);

};