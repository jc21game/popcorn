#include "Salt.h"
#include "PlayScene_S.h"
#include "Score_S.h"
#include "ResultScore.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"
#include "Engine/Direct3D.h"
#include "Engine/Transform.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Salt::Salt(GameObject * parent)
	:GameObject(parent, "Salt"), hSound_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Salt::~Salt()
{
}

//初期化
void Salt::Initialize()
{
	PlayScene_S* pPlayS = (PlayScene_S*)FindObject("PlayScene_S");
	hPict_[0] = Image::Load("Corn.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("PopCorn.png");
	assert(hPict_[1] >= 0);
	//サウンドデータのロード
	hSound_ = Audio::Load("sound.wav");
	assert(hSound_ >= 0);
	transform_.position_.vecY = 1.0f;
	//コーンが降ってくる位置をランダムにする
	switch (pPlayS->line_)
	{
	case 1:rNumber_ = rand() % 2 - 9; break;//-9 〜 -8
	case 2:rNumber_ = rand() % 3 - 6; break;//-6 〜 -4
	case 3:rNumber_ = rand() % 3 - 2; break;//-2 〜 0
	case 4:rNumber_ = rand() % 3 + 2; break;//2 〜 4
	case 5:rNumber_ = rand() % 3 + 7;//7 〜 9
	}
	
	
	transform_.position_.vecX = rNumber_ * 0.1f;

	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);


}

//更新
void Salt::Update()
{
	if (flag)
	{
		if (++Count_ > 5)
		{
			KillMe();
		}
	}
	transform_.position_.vecY -= CornSpeed_;  //コーンを落とす
	PlayScene_S* pPlayS = (PlayScene_S*)FindObject("PlayScene_S");
	Score_S* pScoreS = (Score_S*)FindObject("Score_S");

	//マウス(左)が押されたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの座標を取得
		MousePos_ = Input::GetMousePosition();
		//カップよりも上の場所で押されているか(Judge関数はReleaseの下に記載しています)
		if (MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 245 && Judge(MousePos_, transform_.position_) && pPlayS->clickflag_)
		{
			Audio::Play(hSound_);
			pPlayS->CountUp();
			flag_ = 1;
			ResultScore::Popcorn -= 1;

			if (pPlayS->GetComboCount() < 20)
			{
				pScoreS->ScoreUp(1);
			}
			else if (pPlayS->GetComboCount() >= 20 && pPlayS->GetComboCount() < 50)
			{
				pScoreS->ScoreUp(2);
			}
			else if (pPlayS->GetComboCount() >= 50 && pPlayS->GetComboCount() <= 100)
			{
				pScoreS->ScoreUp(3);
			}

			flag = true;

		}

	}

	//弾がある程度、下に行ったら消える
	if (transform_.position_.vecY < -1.0f)
	{
		pPlayS->ZeroCount();
		KillMe();
	}
}

//描画
void Salt::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Salt::Release()
{
}

//マウスとコーンの当たり判定
bool Salt::Judge(XMVECTOR mouse, XMVECTOR Salt)
{
	if ((mouse.vecX - ConversionX(Salt.vecX)) *(mouse.vecX - ConversionX(Salt.vecX)) + (mouse.vecY - ConversionY(Salt.vecY)) * (mouse.vecY - ConversionY(Salt.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float Salt::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xSaltCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xSaltCenter_;
	}
	else if (xPos > 0)
	{
		xSaltCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xSaltCenter_;
	}
	else if (xPos == 0)
	{
		xSaltCenter_ = DispCenter_.vecX;
		return xSaltCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float Salt::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		ySaltCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return ySaltCenter_;
	}
	else if (yPos < 0)
	{
		ySaltCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return ySaltCenter_;
	}
	else if (yPos == 0)
	{
		ySaltCenter_ = DispCenter_.vecY;
		return ySaltCenter_;
	}
}
