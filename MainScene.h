#pragma once
#include "Engine/GameObject.h"

class MainScene : public GameObject
{
private:




public:
	//Constructor
	//Argument�Fparent  ParentObject�iSceneManager�j
	MainScene(GameObject* parent);

	~MainScene();

	//Initialize
	void Initialize() override;

	//Update
	void Update() override;

	//Drawing
	void Draw() override;

	//Release
	void Release() override;
};
