#include "Cup.h"
#include "PlayScene_C.h"
#include "Engine/Image.h"



//コンストラクタ
Cup::Cup(GameObject * parent)
	:GameObject(parent, "Cup"), hPict_(-1)
{
}

//デストラクタ
Cup::~Cup()
{
}

//初期化
void Cup::Initialize()
{
	//カップ画像のロード
	hPict_ = Image::Load("Cup.png");
	assert(hPict_ >= 0);
	transform_.position_.vecY = -0.2f;
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);
	
}

//更新
void Cup::Update()
{
	PlayScene_C* pPlayC = (PlayScene_C*)FindObject("PlayScene_C");
	if (pPlayC->GetCount() > 19) hPict_ = Image::Load("cuptwofive.png");
	assert(hPict_ >= 0);
}

//描画
void Cup::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Cup::Release()
{
}

