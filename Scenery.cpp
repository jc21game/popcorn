#include "Scenery.h"
#include "Engine/Image.h"

//コンストラクタ
Scenery::Scenery(GameObject * parent)
	:GameObject(parent, "Scenery"), hPict_(-1)
{
}

//デストラクタ
Scenery::~Scenery()
{
}

//初期化
void Scenery::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("CornBack.png");
	assert(hPict_ >= 0);
	transform_.scale_ = XMVectorSet(2.0f, 0.9f, 0, 1);
}

//更新
void Scenery::Update()
{

}

//描画
void Scenery::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void Scenery::Release()
{
}