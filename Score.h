#pragma once
#include "Engine/GameObject.h"

//コンボを管理するクラス
class Score : public GameObject
{
	int hPict_[10];    //画像番号 
	int tenhPict_[10];
	int handhPict_[2];

	Transform tentrans_;
	Transform handtrans_;

	int Score_ = 0;

	char n = 0;
	char t = 0;
	char h = 0;

public:
	//コンストラクタ
	Score(GameObject* parent);

	//デストラクタ
	~Score();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void ScoreUp(int num);


};