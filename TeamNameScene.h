#pragma once
#include "Engine/GameObject.h"


//チームネームを管理するクラス
class TeamNameScene : public GameObject
{
	int hPict_;
public:
	//コンストラクタ
	TeamNameScene(GameObject* parent);

	//デストラクタ
	~TeamNameScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
