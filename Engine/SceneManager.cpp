#include "sceneManager.h"
#include "../TeamNameScene.h"
#include "../TitleScene.h"
#include "../SelectScene.h"
#include "../HomeScene.h"
#include "../PlayScene_C.h"
#include "../PlayScene_B.h"
#include "../PlayScene_S.h"
#include "../Result.h"
#include "../EndlessScene.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"


//コンストラクタ
SceneManager::SceneManager(GameObject * parent)
	: GameObject(parent, "SceneManager")
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	currentSceneID_ = SCENE_ID_RESULT;
	nextSceneID_ = currentSceneID_;
	Instantiate<Result>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (currentSceneID_ != nextSceneID_)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Audio::AllRelease();
		Model::AllRelease();
		Image::AllRelease();

		//次のシーンを作成
		switch (nextSceneID_)
		{
		case SCENE_ID_SPRITE: Instantiate<TeamNameScene>(this); break;
		case SCENE_ID_TITLE:  Instantiate<TitleScene>(this); break;
		case SCENE_ID_SELECT: Instantiate<SelectScene>(this); break;
		case SCENE_ID_HOME:   Instantiate<HomeScene>(this); break;
		case SCENE_ID_PLAY_C:   Instantiate<PlayScene_C>(this); break;
		case SCENE_ID_PLAY_B:   Instantiate<PlayScene_B>(this); break;
		case SCENE_ID_PLAY_S:   Instantiate<PlayScene_S>(this); break;
		case SCENE_ID_RESULT:   Instantiate<Result>(this); break;
		case SCENE_ID_END:      Instantiate<EndlessScene>(this); break;
		}

		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}