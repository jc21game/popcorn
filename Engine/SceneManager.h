#pragma once
#include "global.h"
#include "GameObject.h"

//ゲームに登場するシーン
enum SCENE_ID
{
	SCENE_ID_SPRITE = 0,
	SCENE_ID_TITLE	= 1,
	SCENE_ID_SELECT = 2,
	SCENE_ID_HOME	= 3,
	SCENE_ID_PLAY_C = 4,
	SCENE_ID_PLAY_B = 5,
	SCENE_ID_PLAY_S = 6,
	SCENE_ID_RESULT = 7,
	SCENE_ID_END    = 8,

};

//-----------------------------------------------------------
//シーン切り替えを担当するオブジェクト
//-----------------------------------------------------------
class SceneManager : public GameObject
{
public:

	//コンストラクタ
	//引数：parent	親オブジェクト（基本的にゲームマネージャー）
	SceneManager(GameObject* parent);

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;

	//シーン切り替え（実際に切り替わるのはこの次のフレーム）
	//引数：next	次のシーンのID
	void ChangeScene(SCENE_ID next);

private:
	SCENE_ID currentSceneID_;	//現在のシーン
	SCENE_ID nextSceneID_;		//次のシーン

};