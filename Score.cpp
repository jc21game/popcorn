#include "Score.h"
#include "PlayScene_C.h"
#include "Engine/Image.h"

Score::Score(GameObject * parent) :
	GameObject(parent, "Score")
{
	for (int i = 0; i < 10; i++)
	{
		hPict_[i] = -1;
		tenhPict_[i] = -1;
	}

	for (int i = 0; i < 2; i++)
	{
		handhPict_[i] = -1;
	}
}

Score::~Score()
{
}

void Score::Initialize()
{
	for (int i = 0; i < 10; i++)
	{
		switch (i)
		{
		case 0:
			hPict_[i] = Image::Load("zero.png");
			tenhPict_[i] = Image::Load("zero.png");
			handhPict_[i] = Image::Load("zero.png");
			break;
		case 1:
			hPict_[i] = Image::Load("one.png");
			tenhPict_[i] = Image::Load("one.png");
			handhPict_[i] = Image::Load("one.png");
			break;
		case 2:
			hPict_[i] = Image::Load("two.png");
			tenhPict_[i] = Image::Load("two.png");
			break;
		case 3:
			hPict_[i] = Image::Load("three.png");
			tenhPict_[i] = Image::Load("three.png");
			break;
		case 4:
			hPict_[i] = Image::Load("four.png");
			tenhPict_[i] = Image::Load("four.png");
			break;
		case 5:
			hPict_[i] = Image::Load("five.png");
			tenhPict_[i] = Image::Load("five.png");
			break;
		case 6:
			hPict_[i] = Image::Load("six.png");
			tenhPict_[i] = Image::Load("six.png");
			break;
		case 7:
			hPict_[i] = Image::Load("seven.png");
			tenhPict_[i] = Image::Load("seven.png");
			break;
		case 8:
			hPict_[i] = Image::Load("eight.png");
			tenhPict_[i] = Image::Load("eight.png");
			break;
		case 9:
			hPict_[i] = Image::Load("nine.png");
			tenhPict_[i] = Image::Load("nine.png");
		}
		assert(hPict_[i] >= 0);
		assert(tenhPict_[i] >= 0);
		assert(handhPict_[i] >= 0);
	}

	transform_.position_ = XMVectorSet(0.9f, 0.9f, 0, 0);
	transform_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	tentrans_.position_ = XMVectorSet(0.85f, 0.9f, 0, 0);
	tentrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 1);
	handtrans_.position_ = XMVectorSet(0.8f, 0.9f, 0, 0);
	handtrans_.scale_ = XMVectorSet(2.0f, 2.0f, 0, 0);
}

void Score::Update()
{
	if (Score_ == 0)
	{
		n = 0;
		t = 0;
		h = 0;
	}
	else if (Score_ < 10)
	{
		switch (Score_)
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9; break;
		}
	}
	else if (Score_ == 10)
	{
		t = 1;
		n = 0;
	}
	else if (Score_ < 20)
	{
		t = 1;
		switch (Score_ % 10)
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9; break;
		}
	}
	else if (Score_ == 20)
	{
		t = 2;
		n = 0;
	}
	else if (Score_ < 30)
	{
		t = 2;
		switch (Score_ % 20)
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9; break;
		}
	}
	else if (Score_ == 30)
	{
		t = 3;
		n = 0;
	}
	else if (Score_ < 40)
	{
		t = 3;
		switch (Score_ % 30)
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9; break;
		}
	}

	else if (Score_ == 40)
	{
		t = 4;
		n = 0;
	}
	else if (Score_ < 50)
	{
		t = 3;
		switch (Score_ % 40)
		{
		case 0:n = 0; break;
		case 1:n = 1; break;
		case 2:n = 2; break;
		case 3:n = 3; break;
		case 4:n = 4; break;
		case 5:n = 5; break;
		case 6:n = 6; break;
		case 7:n = 7; break;
		case 8:n = 8; break;
		case 9:n = 9; break;
		}
	}
}

void Score::Draw()
{
	Image::SetTransform(hPict_[n], transform_);
	Image::Draw(hPict_[n]);
	Image::SetTransform(tenhPict_[t], tentrans_);
	Image::Draw(tenhPict_[t]);
	Image::SetTransform(handhPict_[h], handtrans_);
	Image::Draw(handhPict_[h]);
}

void Score::Release()
{
}

void Score::ScoreUp(int num)
{
	Score_ += num;
}
