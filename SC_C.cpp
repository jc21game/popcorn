#include "SC_C.h"
#include "PlayScene_C.h"
#include "Score_C.h"
#include "ResultScore.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"
#include "Engine/Direct3D.h"
#include "Engine/Transform.h"
#include "Engine/SceneManager.h"

//コンストラクタ
SC_C::SC_C(GameObject * parent)
	:GameObject(parent, "SC_C"),hSound_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
SC_C::~SC_C()
{
}

//初期化
void SC_C::Initialize()
{
	PlayScene_C* pPlayC = (PlayScene_C*)FindObject("PlayScene_C");
	hPict_[0] = Image::Load("SpecialCorn.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("PopCorn.png");
	assert(hPict_[1] >= 0);
	//サウンドデータのロード
	hSound_ = Audio::Load("sound.wav");
	assert(hSound_ >= 0);

	transform_.position_.vecY = 1.0f ;

	rPos_ = rand() % 2 + 1;
	//コーンが降ってくる位置をランダムにする
	switch (rPos_)
	{
	case 1:rNumber_ = rand() % 3 - 6; break;//-6 〜 -4
	case 2:rNumber_ = rand() % 3 + 3; //3 〜 5
	}
	
    transform_.position_.vecX = rNumber_ * 0.1f;
	
	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);
}

//更新
void SC_C::Update()
{
	if (flag)
	{
		if (++Count_ > 10)
		{
			KillMe();
		}
	}
	transform_.position_.vecY -= CornSpeed_;  //コーンを落とす
	PlayScene_C* pPlayC = (PlayScene_C*)FindObject("PlayScene_C");
	Score_C* pScoreC = (Score_C*)FindObject("Score_C");

	//マウス(左)が押されたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの座標を取得
		MousePos_ = Input::GetMousePosition();
		//カップよりも上の場所で押されているか(Judge関数はReleaseの下に記載しています)
		if (MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 245 && Judge(MousePos_, transform_.position_))
		{
			Audio::Play(hSound_);
			flag_ = 1;
			pScoreC->ScoreUp(500);
			pPlayC->clickflag_ = false;
			flag = true;

		}
	}

    //弾がある程度、下に行ったら消える
	if (transform_.position_.vecY < -1.0f)  
	{
		KillMe();
	}

	if (ResultScore::Popcorn < 0)
	{
		KillMe();
	}

}

//描画
void SC_C::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void SC_C::Release()
{
}

//マウスとコーンの当たり判定
bool SC_C::Judge(XMVECTOR mouse, XMVECTOR Caramel)
{
	if ((mouse.vecX - ConversionX(Caramel.vecX)) *(mouse.vecX - ConversionX(Caramel.vecX)) + (mouse.vecY - ConversionY(Caramel.vecY)) * (mouse.vecY - ConversionY(Caramel.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float SC_C::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xSC_CCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xSC_CCenter_;
	}
	else if (xPos > 0)
	{
		xSC_CCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xSC_CCenter_;
	}
	else if (xPos == 0)
	{
		xSC_CCenter_ = DispCenter_.vecX;
		return xSC_CCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float SC_C::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		ySC_CCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return ySC_CCenter_;
	}
	else if (yPos < 0)
	{
		ySC_CCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return ySC_CCenter_;
	}
	else if (yPos == 0)
	{
		ySC_CCenter_ = DispCenter_.vecY;
		return ySC_CCenter_;
	}
}
