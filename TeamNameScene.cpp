#include "TeamNameScene.h"
#include "Engine/Image.h"
#include "Engine/SceneManager.h"
#include "Engine/input.h"

//コンストラクタ
TeamNameScene::TeamNameScene(GameObject * parent)
	: GameObject(parent, "TeamNameScene"), hPict_(-1)
{
}

//デストラクタ
TeamNameScene::~TeamNameScene()
{
}

//初期化
void TeamNameScene::Initialize()
{
	//チームネームロゴ表示
	hPict_ = Image::Load("TeamName.png");
	assert(hPict_ >= 0);
	transform_.scale_ = { 3.2f,3.2f,0,1 };
}

//更新
void TeamNameScene::Update()
{
	//マウス(右)を押したらスタート
	if (Input::IsMouseButtonDown(0))
	{
		//シーンの切り替え->タイトルシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void TeamNameScene::Draw()
{

	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TeamNameScene::Release()
{
}
