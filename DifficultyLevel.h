#pragma once
#include "Engine/GameObject.h"

class DifficultyLevel : public GameObject
{
private:
	int ShPict_;    //しお画像変数
	int BhPict_;    //バター画像変数
	int ChPict_;    //キャラメル画像変数

	Transform				Btransform_; //バターの画像位置
	Transform				Ctransform_; //キャラメルの画像位置
	
	XMVECTOR				MousePos_;   //マウスの位置座標を入れる変数

	

public:
	//コンストラクタ
	DifficultyLevel(GameObject* parent);
	//デストラクタ
	~DifficultyLevel();
	//初期化
	void Initialize() override;
	//更新
	void Update() override;
	//描画
	void Draw() override;
	//解放
	void Release() override;

	
};