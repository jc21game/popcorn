#include "Caramel.h"
#include "PlayScene_C.h"
#include "Score_C.h"
#include "ResultScore.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine/Audio.h"
#include "Engine/Direct3D.h"
#include "Engine/Transform.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Caramel::Caramel(GameObject * parent)
	:GameObject(parent, "Caramel"),hSound_(-1)
{
	for (int i = 0; i < 2; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Caramel::~Caramel()
{
}

//初期化
void Caramel::Initialize()
{
	PlayScene_C* pPlayC = (PlayScene_C*)FindObject("PlayScene_C");
	hPict_[0] = Image::Load("Corn.png");
	assert(hPict_[0] >= 0);
	hPict_[1] = Image::Load("PopCorn.png");
	assert(hPict_[1] >= 0);
	//サウンドデータのロード
	hSound_ = Audio::Load("sound.wav");
	assert(hSound_ >= 0);

	transform_.position_.vecY = 1.0f ;
	//コーンが降ってくる位置をランダムにする
	switch (pPlayC->line_)
	{
	case 1:rNumber_ = rand() % 3 -9; break; //-9 〜 -7
	case 2:rNumber_ = rand() % 5 - 3; break;//-3 〜 2
	case 3:rNumber_ = rand() % 4 + 6;//6 〜 9
	}
	
    transform_.position_.vecX = rNumber_ * 0.1f;
	
	DispCenter_ = XMVectorSet(Direct3D::screenWidth_ / 2, Direct3D::screenHeight_ / 2, 0, 0);
}

//更新
void Caramel::Update()
{
	if (flag)
	{
		if (++Count_ > 10)
		{
			KillMe();
		}
	}
	transform_.position_.vecY -= CornSpeed_;  //コーンを落とす
	PlayScene_C* pPlayC = (PlayScene_C*)FindObject("PlayScene_C");
	Score_C* pScoreC = (Score_C*)FindObject("Score_C");

	//マウス(左)が押されたら
	if (Input::IsMouseButtonDown(0))
	{
		//マウスの座標を取得
		MousePos_ = Input::GetMousePosition();
		//カップよりも上の場所で押されているか(Judge関数はReleaseの下に記載しています)
		if (MousePos_.vecY < (Direct3D::screenHeight_ / 2) + 245 && Judge(MousePos_, transform_.position_) && pPlayC->clickflag_)
		{
			Audio::Play(hSound_);
			pPlayC->CountUp();
			flag_ = 1;
			ResultScore::Popcorn -= 1;

			if (pPlayC->GetCount() < 20)
			{
				pScoreC->ScoreUp(1);
			}
			else if (pPlayC->GetCount() >= 20 && pPlayC->GetCount() < 50)
			{
				pScoreC->ScoreUp(2);
			}

			flag = true;

		}
	}

    //弾がある程度、下に行ったら消える
	if (transform_.position_.vecY < -1.0f)  
	{
		pPlayC->ZeroCount();
		KillMe();
	}

}

//描画
void Caramel::Draw()
{
	Image::SetTransform(hPict_[flag_], transform_);
	Image::Draw(hPict_[flag_]);
}

//開放
void Caramel::Release()
{
}

//マウスとコーンの当たり判定
bool Caramel::Judge(XMVECTOR mouse, XMVECTOR Caramel)
{
	if ((mouse.vecX - ConversionX(Caramel.vecX)) *(mouse.vecX - ConversionX(Caramel.vecX)) + (mouse.vecY - ConversionY(Caramel.vecY)) * (mouse.vecY - ConversionY(Caramel.vecY)) <= 33 * 33)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//当たり判定---コーンのX座標の値をスクリーン座標に変換
float Caramel::ConversionX(float xPos)
{
	if (xPos < 0)
	{
		xCaramelCenter_ = DispCenter_.vecX * (1.0f + xPos);
		return xCaramelCenter_;
	}
	else if (xPos > 0)
	{
		xCaramelCenter_ = DispCenter_.vecX + (DispCenter_.vecX - DispCenter_.vecX * (1.0f - xPos));
		return xCaramelCenter_;
	}
	else if (xPos == 0)
	{
		xCaramelCenter_ = DispCenter_.vecX;
		return xCaramelCenter_;
	}
}

//当たり判定---コーンのY座標の値をスクリーン座標に変換
float Caramel::ConversionY(float yPos)
{
	if (yPos > 0)
	{
		yCaramelCenter_ = DispCenter_.vecY * (1.0f - yPos);
		return yCaramelCenter_;
	}
	else if (yPos < 0)
	{
		yCaramelCenter_ = DispCenter_.vecY + (DispCenter_.vecY - DispCenter_.vecY * (1.0f + yPos));
		return yCaramelCenter_;
	}
	else if (yPos == 0)
	{
		yCaramelCenter_ = DispCenter_.vecY;
		return yCaramelCenter_;
	}
}
