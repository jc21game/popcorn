#pragma once
#include "Engine/GameObject.h"

//コーンを管理するクラス
class Cup : public GameObject
{
	int hPict_;    //画像番号 

public:
	//コンストラクタ
	Cup(GameObject* parent);

	//デストラクタ
	~Cup();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};