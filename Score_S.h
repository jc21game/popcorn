#pragma once
#include "Engine/GameObject.h"

//コンボを管理するクラス
class Score_S : public GameObject
{
	int fPict_;
	int hPict_[10];    //画像番号 
	int tenhPict_[10];
	int hundhPict_[10];
	int thoushPict_[10];
	int tenthoushPict_[10];
	int hund_thous_hPict_[10];
	int millionhPict_[10];

	int aScore_[7];

	Transform ftrans_;
	Transform tentrans_;
	Transform hundtrans_;
	Transform thoustrans_;
	Transform tenthoustrans_;
	Transform hund_thous_trans_;
	Transform milliontrans_;

	

	char n = 0;
	char t = 0;
	char h = 0;
	char th = 0;
	char tt = 0;
	char ht = 0;
	char m = 0;

public:
	//コンストラクタ
	Score_S(GameObject* parent);

	//デストラクタ
	~Score_S();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void ScoreUp(int num);


};