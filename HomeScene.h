#pragma once
#include "Engine/GameObject.h"

class HomeScene : public GameObject
{
private:
	
public:
	//コンストラクタ
	HomeScene(GameObject* parent);

	//デストラクタ
	~HomeScene();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;
};
