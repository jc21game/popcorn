#include "ReleaseEffect.h"
#include "LockManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"



//コンストラクタ
ReleaseEffect::ReleaseEffect(GameObject * parent)
	:GameObject(parent, "ReleaseEffect"),hPict_(-1)
{
	
}

//デストラクタ
ReleaseEffect::~ReleaseEffect()
{
}

//初期化
void ReleaseEffect::Initialize()
{
	hPict_ = Image::Load("ExtraUnLock.png");
	assert(hPict_ >= 0);

}

//更新
void ReleaseEffect::Update()
{
	if (Input::IsMouseButtonDown(1))
	{
		LockManager::disLock = false;
		KillMe();
	}
}

//描画
void ReleaseEffect::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void ReleaseEffect::Release()
{
}

