#include "Combo_E.h"
#include "EndlessScene.h"
#include "Engine/Image.h"

Combo_E::Combo_E(GameObject * parent) :
	GameObject(parent, "Combo_E")
{
	for (int i = 0; i < 10; i++)
	{
		hPict_[i] = -1;
		tenhPict_[i] = -1;
		hundhPict_[i] = -1;
	}

	for (int i = 0; i < 2; i++)
	{
		aScore_[i] = -1;
	}
}

Combo_E::~Combo_E()
{
}

void Combo_E::Initialize()
{
	for (int i = 0; i < 10; i++)
	{
		switch (i)
		{
		case 0:
			hPict_[i] = Image::Load("zero.png");
			tenhPict_[i] = Image::Load("zero.png");
			hundhPict_[i] = Image::Load("zero.png");
			break;
		case 1:
			hPict_[i] = Image::Load("one.png");
			tenhPict_[i] = Image::Load("one.png");
			hundhPict_[i] = Image::Load("one.png");
			break;
		case 2:
			hPict_[i] = Image::Load("two.png");
			tenhPict_[i] = Image::Load("two.png");
			hundhPict_[i] = Image::Load("two.png");
			break;
		case 3:
			hPict_[i] = Image::Load("three.png");
			tenhPict_[i] = Image::Load("three.png");
			hundhPict_[i] = Image::Load("three.png");
			break;
		case 4:
			hPict_[i] = Image::Load("four.png");
			tenhPict_[i] = Image::Load("four.png");
			hundhPict_[i] = Image::Load("four.png");
			break;
		case 5:
			hPict_[i] = Image::Load("five.png");
			tenhPict_[i] = Image::Load("five.png");
			hundhPict_[i] = Image::Load("five.png");
			break;
		case 6:
			hPict_[i] = Image::Load("six.png");
			tenhPict_[i] = Image::Load("six.png");
			hundhPict_[i] = Image::Load("six.png");
			break;
		case 7:
			hPict_[i] = Image::Load("seven.png");
			tenhPict_[i] = Image::Load("seven.png");
			hundhPict_[i] = Image::Load("seven.png");
			break;
		case 8:
			hPict_[i] = Image::Load("eight.png");
			tenhPict_[i] = Image::Load("eight.png");
			hundhPict_[i] = Image::Load("eight.png");
			break;
		case 9:
			hPict_[i] = Image::Load("nine.png");
			tenhPict_[i] = Image::Load("nine.png");
			hundhPict_[i] = Image::Load("nine.png");
		}
		assert(hPict_[i] >= 0);
		assert(tenhPict_[i] >= 0);
		assert(hundhPict_[i] >= 0);
	}

	transform_.position_ = XMVectorSet(0.15f, 0, 0, 0);
	transform_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	tentrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 1);
	handtrans_.position_ = XMVectorSet(-0.15f, 0, 0, 0);
	handtrans_.scale_ = XMVectorSet(3.0f, 3.0f, 0, 0);
}

void Combo_E::Update()
{
	EndlessScene* pEnd = (EndlessScene*)FindObject("EndlessScene");
	ComboCounter_ = pEnd->GetComboCount();

	aScore_[0] = ComboCounter_ % 1000 / 100;
	aScore_[1] = ComboCounter_ % 100 / 10;
	aScore_[2] = ComboCounter_ % 10;

	if (ComboCounter_ == 0)
	{
		n = 0;
		t = 0;
		h = 0;
	}

	switch (aScore_[0])
	{
	case 0:h = 0; break;
	case 1:h = 1; break;
	case 2:h = 2; break;
	case 3:h = 3; break;
	case 4:h = 4; break;
	case 5:h = 5; break;
	case 6:h = 6; break;
	case 7:h = 7; break;
	case 8:h = 8; break;
	case 9:h = 9;
	}

	switch (aScore_[1])
	{
	case 0:t = 0; break;
	case 1:t = 1; break;
	case 2:t = 2; break;
	case 3:t = 3; break;
	case 4:t = 4; break;
	case 5:t = 5; break;
	case 6:t = 6; break;
	case 7:t = 7; break;
	case 8:t = 8; break;
	case 9:t = 9;
	}

	switch (aScore_[2])
	{
	case 0:n = 0; break;
	case 1:n = 1; break;
	case 2:n = 2; break;
	case 3:n = 3; break;
	case 4:n = 4; break;
	case 5:n = 5; break;
	case 6:n = 6; break;
	case 7:n = 7; break;
	case 8:n = 8; break;
	case 9:n = 9;
	}
	
}

void Combo_E::Draw()
{
	Image::SetTransform(hPict_[n], transform_);
	Image::Draw(hPict_[n]);
	Image::SetTransform(tenhPict_[t], tentrans_);
	Image::Draw(tenhPict_[t]);
	Image::SetTransform(hundhPict_[h], handtrans_);
	Image::Draw(hundhPict_[h]);
}

void Combo_E::Release()
{
}
