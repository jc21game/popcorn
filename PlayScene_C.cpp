#include "PlayScene_C.h"
#include "DifficultyLevel.h"
#include "BackGroundimage.h"
#include "FryingPan.h"
#include "Cup_C.h"
#include "Caramel.h"
#include "SC_C.h"
#include "Combo_C.h"
#include "Score_C.h"
#include "Timer.h"
#include "ResultScore.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
PlayScene_C::PlayScene_C(GameObject * parent)
	:GameObject(parent, "PlayScene_C")
{

}

//デストラクタ
PlayScene_C::~PlayScene_C()
{
}

//初期化
void PlayScene_C::Initialize()
{
	ResultScore::Popcorn = 2;

	//背景を表示させる
	Instantiate<BackGroundimage>(this);

	//フライパンを表示させる
	Instantiate<FryingPan>(this);

	//カップを表示させる
	Instantiate<Cup_C>(this);

	//コンボを表示させる
	Instantiate<Combo_C>(this);

	//スコアを表示させる
	Instantiate<Score_C>(this);
	

	//コーンを表示させる
	for (int i = 0; i < 3; i++)
	{
		Instantiate<Caramel>(this);
		line_ += 1;
	}

	//タイマーを表示させる
	Instantiate<Timer>(this);

}

//更新
void PlayScene_C::Update()
{
	
	if (FindChildObject("Caramel") == nullptr)
	{
		ResultScore::Popcorn = 2;
		if (FindChildObject("Timer") == nullptr)
		{
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_RESULT);
		}
		else
		{
			if (LoopCounter_ % 5 == 0)Instantiate<SC_C>(this);
			line_ = 1;
			for (int i = 0; i < 3; i++)
			{
				Instantiate<Caramel>(this);
				line_ += 1;
			}
			LoopCounter_ += 1;
			clickflag_ = true;
		}
		
	}

}

//描画
void PlayScene_C::Draw()
{

}

//解放
void PlayScene_C::Release()
{
}

void PlayScene_C::CountUp()
{
	CupCounter_ = CupCounter_ + 1;
	ComboCounter_ = ComboCounter_ + 1;
	ResultScore::MaxCombo_C = ComboCounter_;
}

int PlayScene_C::GetCount()
{
	return CupCounter_;
}

int PlayScene_C::GetComboCount()
{
	return ComboCounter_;
}

void PlayScene_C::ZeroCount()
{
	if (ResultScore::MaxCombo_C == 0)
	{
		ResultScore::MaxCombo_C = ComboCounter_;
	}
	else
	{
		if (ResultScore::MaxCombo_C < ComboCounter_)
		{
			ResultScore::MaxCombo_C = ComboCounter_;
		}
	}
	ComboCounter_ = 0;
}
