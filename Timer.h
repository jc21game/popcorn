#pragma once
#include "Engine/GameObject.h"

//コンボを管理するクラス
class Timer : public GameObject
{
	int hPict_[31];    //画像番号 
	

    int time;

public:
	char TimerFlag_ = 30;
	//コンストラクタ
	Timer(GameObject* parent);

	//デストラクタ
	~Timer();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void CountDown();

};


